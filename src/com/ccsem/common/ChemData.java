package com.ccsem.common;

public class ChemData {
    float xrf, ccs, org;

    public float getXrf() {
        return xrf;
    }

    public void setXrf(float xrf) {
        this.xrf = xrf;
    }

    public float getCcs() {
        return ccs;
    }

    public void setCcs(float ccs) {
        this.ccs = ccs;
    }

    public float getOrg() {
        return org;
    }

    public void setOrg(float org) {
        this.org = org;
    }
}
