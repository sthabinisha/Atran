package com.ccsem.common;

import java.util.List;

public class CCSEMStorageUnit {

    public float Mg;
    public float Al;
    public float Si;
    public float P;
    public float S;
    public float Cl;
    public float K;
    public float Ca;
    public float Fe;
    public float Ba;
    public float Ti;
    public float xc;
    public float yc;
    public float NA;
    public float avgdiameter;
    public float minDiameter;
    public float maxDiameter;

    public float newDiameter;
    public float newArea;
    public float perim;
    public float shape;

    public float area;
    public int locLib;
    public float phase;
    public float frager;
    public int frame;
    public float diaRatio;
    public int point;
    public int ctype;

    public int cts;
    public float xcoord;

    public float ycoord;
    public List<CCSEMStorageUnit> magnification1;
    public List<CCSEMStorageUnit> magnification2;

    public List<CCSEMStorageUnit> getMagnification1() {
        return magnification1;
    }

    public void setMagnification1(List<CCSEMStorageUnit> magnification1) {
        this.magnification1 = magnification1;
    }

    public List<CCSEMStorageUnit> getMagnification2() {
        return magnification2;
    }

    public void setMagnification2(List<CCSEMStorageUnit> magnification2) {
        this.magnification2 = magnification2;
    }

    public float areaCorrected;

    public float getAreaCorrected() {
        return areaCorrected;
    }

    public void setAreaCorrected(float areaCorrected) {
        this.areaCorrected = areaCorrected;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public float weight;

//    public float getNewDiameter() {
//        return newDiameter;
//    }
//
//    public void setNewDiameter(float newDiameter) {
//        this.newDiameter = newDiameter;
//    }

//    public float getNewArea() {
//        return newArea;
//    }
//
//    public void setNewArea(float newArea) {
//        this.newArea = newArea;
//    }

    public float getMinDiameter() {
        return minDiameter;
    }

    public void setMinDiameter(float minDiameter) {
        this.minDiameter = minDiameter;
    }

    public float getMaxDiameter() {
        return maxDiameter;
    }

    public void setMaxDiameter(float maxDiameter) {
        this.maxDiameter = maxDiameter;
    }

    public float getPerim() {
        return perim;
    }

    public void setPerim(float perim) {
        this.perim = perim;
    }

    public float getShape() {
        return shape;
    }

    public void setShape(float shape) {
        this.shape = shape;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public int getCtype() {
        return ctype;
    }

    public void setCtype(int ctype) {
        this.ctype = ctype;
    }

    public int getCts() {
        return cts;
    }

    public void setCts(int cts) {
        this.cts = cts;
    }

    public float getXcoord() {
        return xcoord;
    }

    public void setXcoord(float xcoord) {
        this.xcoord = xcoord;
    }

    public float getYcoord() {
        return ycoord;
    }

    public void setYcoord(float ycoord) {
        this.ycoord = ycoord;
    }

    public int getFrame() {
        return frame;
    }

    public void setFrame(int frame) {
        this.frame = frame;
    }



    public CCSEMStorageUnit() {

    }

    public int getPoint() {
        return point;
    }




    public float getMg() {
        return Mg;
    }

    public void setMg(float mg) {
        Mg = mg;
    }

    public float getAl() {
        return Al;
    }

    public void setAl(float al) {
        Al = al;
    }

    public float getSi() {
        return Si;
    }

    public void setSi(float si) {
        Si = si;
    }

    public float getP() {
        return P;
    }

    public void setP(float p) {
        P = p;
    }

    public float getS() {
        return S;
    }

    public void setS(float s) {
        S = s;
    }

    public float getCl() {
        return Cl;
    }

    public void setCl(float cl) {
        Cl = cl;
    }

    public float getK() {
        return K;
    }

    public void setK(float k) {
        K = k;
    }

    public float getCa() {
        return Ca;
    }

    public void setCa(float ca) {
        Ca = ca;
    }

    public float getFe() {
        return Fe;
    }

    public void setFe(float fe) {
        Fe = fe;
    }

    public float getBa() {
        return Ba;
    }

    public void setBa(float ba) {
        Ba = ba;
    }

    public float getTi() {
        return Ti;
    }

    public void setTi(float ti) {
        Ti = ti;
    }

    public float getXc() {
        return xc;
    }

    public void setXc(float xc) {
        this.xc = xc;
    }

    public float getYc() {
        return yc;
    }

    public void setYc(float yc) {
        this.yc = yc;
    }

    public float getNA() {
        return NA;
    }

    public void setNA(float NA) {
        this.NA = NA;
    }

    public float getAvgDiameter() {
        return avgdiameter;
    }

    public void setAvgDiameter(float diameter) {
        this.avgdiameter = diameter;
    }

    public float getArea() {
        return area;
    }

    public void setArea(float area) {
        this.area = area;
    }

    public int getLocLib() {
        return locLib;
    }

    public void setLocLib(int locLib) {
        this.locLib = locLib;
    }

    public float getPhase() {
        return phase;
    }

    public void setPhase(float phase) {
        this.phase = phase;
    }

    public float getFrager() {
        return frager;
    }

    public void setFrager(float frager) {
        this.frager = frager;
    }

    public float getDiaRatio() {
        return diaRatio;
    }

    public void setDiaRatio(float diaRatio) {
        this.diaRatio = diaRatio;
    }








}
