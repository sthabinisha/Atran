package com.ccsem.common;

/*Compound.cpp
 * Description: Mineral classification requirements, used in atran1 and 5
 *
 * Author: Original by Tom Erickson, c version written by Shaker.
 * Modifications:Memory setup is removed as for java it is not required
 * Re-Modified by Binisha and copied in Java format
 *
 * Modification  2020-updated based on the elements

 *  Company: Energy & Environmental Research Center
 * */
public class CompoundName {

	public String Name(int Phase) {
		if (Phase == 1) {
			return "QUARTZ";
		} else if (Phase == 2) {
			return "IRON OXIDE";
		} else if (Phase == 3) {
			return "PERICLASE";

		} else if (Phase == 4) {
			return "RUTILE";

		} else if (Phase == 5) {
			return "ALUMINA";
		} else if (Phase == 6) {
			return "CALCITE";
		} else if (Phase == 7) {
			return "DOLOMITE";
		} else if (Phase == 8) {
			return "ANKERITE";
		} else if (Phase == 9) {
			return "KAOLINITE";

		} else if (Phase == 10) {
			return "MONTMORILLONITE";

		} else if (Phase == 11) {
			return "K AL-SILICATE";


		} else if (Phase == 12) {
			return "FE AL-SILICATE";


		} else if (Phase == 13) {
			return "CA AL-SILICATE";


		} else if (Phase == 14) {
			return "NA AL-SILICATE";

		} else if (Phase == 15) {
			return "ALUMINOSILICATE";


		} else if (Phase == 16) {
			return "MIXED AL-SILICA";

		} else if (Phase == 17) {
			return "FE SILICATE";

		} else if (Phase == 18) {
			return "CA SILICATE";

		} else if (Phase == 19) {
			return "CA ALUMINATE";

		} else if (Phase == 20) {
			return "PYRITE";

		} else if (Phase == 21) {
			return "PYRRHOTITE";

		} else if (Phase == 22) {
			return "OXIDIZED PYRRHO";

		} else if (Phase == 23) {
			return "GYPSUM";

		} else if (Phase == 24) {
			return "BARITE";
		} else if (Phase == 25) {
			return "APATITE";
		} else if (Phase == 26) {
			return "CA AL-P";
		} else if (Phase == 27) {
			return "KCL";
		} else if (Phase == 28) {
			return "GYPSUM/ BARITE";
		} else if (Phase == 29) {
			return "GYPSUM/ AL-SILIC";
		} else if (Phase == 30) {
			return "SI-RICH";
		} else if (Phase == 31) {
			return "CA-RICH";
		} else if (Phase == 32) {
			return "CA-SI RICH";
		} else if (Phase == 33) {
			return "UNCLASSIFIED";


		}
		return "UNCLASSIFIED";


	}
}