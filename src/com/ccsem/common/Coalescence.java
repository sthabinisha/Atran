package com.ccsem.common;

public class Coalescence {
    public float Mg;
    public float Al;
    public float Si;
    public float P;
    public float S;
    public float Cl;
    public float K;
    public float Ca;
    public float Fe;
    public float Ba;
    public float Ti;
    public float xc;
    public float yc;
    public float NA;
    public int point;
    public int ctype;
    public int phase;

    public int cts;
    public float xcoord;

    public float ycoord;
    public float avgdiameter;
    public float minDiameter;
    public float maxDiameter;
    float pv;
    float shape;
    float perim;
    int cnumb;
    float avgDensity;
    int frame;

    public float getMg() {
        return Mg;
    }

    public void setMg(float mg) {
        Mg = mg;
    }

    public float getAl() {
        return Al;
    }

    public void setAl(float al) {
        Al = al;
    }

    public float getSi() {
        return Si;
    }

    public void setSi(float si) {
        Si = si;
    }

    public float getP() {
        return P;
    }

    public void setP(float p) {
        P = p;
    }

    public float getS() {
        return S;
    }

    public void setS(float s) {
        S = s;
    }

    public float getCl() {
        return Cl;
    }

    public void setCl(float cl) {
        Cl = cl;
    }

    public float getK() {
        return K;
    }

    public void setK(float k) {
        K = k;
    }

    public float getCa() {
        return Ca;
    }

    public void setCa(float ca) {
        Ca = ca;
    }

    public float getFe() {
        return Fe;
    }

    public void setFe(float fe) {
        Fe = fe;
    }

    public float getBa() {
        return Ba;
    }

    public void setBa(float ba) {
        Ba = ba;
    }

    public float getTi() {
        return Ti;
    }

    public void setTi(float ti) {
        Ti = ti;
    }

    public float getXc() {
        return xc;
    }

    public void setXc(float xc) {
        this.xc = xc;
    }

    public float getYc() {
        return yc;
    }

    public void setYc(float yc) {
        this.yc = yc;
    }

    public float getNA() {
        return NA;
    }

    public void setNA(float NA) {
        this.NA = NA;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public int getCtype() {
        return ctype;
    }

    public void setCtype(int ctype) {
        this.ctype = ctype;
    }

    public int getCts() {
        return cts;
    }

    public void setCts(int cts) {
        this.cts = cts;
    }

    public float getXcoord() {
        return xcoord;
    }

    public void setXcoord(float xcoord) {
        this.xcoord = xcoord;
    }

    public float getYcoord() {
        return ycoord;
    }

    public void setYcoord(float ycoord) {
        this.ycoord = ycoord;
    }

    public float getAvgdiameter() {
        return avgdiameter;
    }

    public void setAvgdiameter(float avgdiameter) {
        this.avgdiameter = avgdiameter;
    }

    public float getMinDiameter() {
        return minDiameter;
    }

    public void setMinDiameter(float minDiameter) {
        this.minDiameter = minDiameter;
    }

    public float getMaxDiameter() {
        return maxDiameter;
    }

    public void setMaxDiameter(float maxDiameter) {
        this.maxDiameter = maxDiameter;
    }

    public float getPv() {
        return pv;
    }

    public void setPv(float pv) {
        this.pv = pv;
    }

    public float getShape() {
        return shape;
    }

    public void setShape(float shape) {
        this.shape = shape;
    }

    public float getPerim() {
        return perim;
    }

    public void setPerim(float perim) {
        this.perim = perim;
    }

    public int getCnumb() {
        return cnumb;
    }

    public void setCnumb(int cnumb) {
        this.cnumb = cnumb;
    }

    public float getAvgDensity() {
        return avgDensity;
    }

    public void setAvgDensity(float avgDensity) {
        this.avgDensity = avgDensity;
    }

    public int getFrame() {
        return frame;
    }

    public void setFrame(int frame) {
        this.frame = frame;
    }

    public int getPhase() {
        return phase;
    }

    public void setPhase(int phase) {
        this.phase = phase;
    }
}
