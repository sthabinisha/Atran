package com.ccsem.common;

import java.io.*;
import java.util.Scanner;

public class FileIO {

	public File createFile(String fileName){
		File file = new File(fileName);
		try {
			if (file.createNewFile()) {
				System.out.println("File created: " + file.getName());
			} else{

			}
		} catch (IOException e) {
			e.printStackTrace();
		}


		return file;
	}
	public File tprintf (String fileName, String data ){
		File file = new File(fileName);
		try {
			if (file.createNewFile()) {
				System.out.println("File created: " + file.getName());
			} else {
				//System.out.println("File already exists.");
			}
			FileWriter myWriter = new FileWriter(fileName, true);

			myWriter.write(data);
			myWriter.write("\n");
			myWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}


		return file;
	}

	public boolean tlineByline(String inputFileName, String[] magAll2) {
		// TODO Auto-generated method stub
		File file = new File(inputFileName);
		try {

		FileReader fr=new FileReader(file);   //reads the file
		BufferedReader br=new BufferedReader(fr);  //creates a buffering character input stream
		StringBuffer sb=new StringBuffer();    //constructs a string buffer with no characters

		String line;
		while((line=br.readLine())!=null)  
		{  
		sb.append(line);      //appends line to string buffer  
		sb.append("\n");     //line feed   
		}  
			fr.close();
			System.out.println("Contents of File: ");
			System.out.println(sb.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}    //closes the stream and release the resources  


		//System.out.printf("%s %s %s%n", tokens[0], tokens[1], tokens[2]);		StringBuffer sb=new StringBuffer();  		

		return false;
	}
	public boolean tsanf(String inputFileName, String[] magAll2) {
		// TODO Auto-generated method stub
		 try {
		      File myObj = new File(inputFileName);
		      Scanner myReader = new Scanner(myObj);
		      while (myReader.hasNextLine()) {
		        String data_i = myReader.nextLine();
		        System.out.println(magAll2);
		        return true;
		      }
		      myReader.close();
		    } catch (FileNotFoundException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		    }
		return false;	}
	
	public boolean tsanf(String inputFileName, String name) {
		// TODO Auto-generated method stub
		try {
		      File myObj = new File(inputFileName);
		      Scanner myReader = new Scanner(myObj);
		      while (myReader.hasNextLine()) {
		        String data_i = myReader.nextLine();
		        System.out.println(name);
		        return true;
		      }
		      myReader.close();
		    } catch (FileNotFoundException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		    }
		return false;
	}
	
	public String tgets() {
		Scanner scan = new Scanner(System.in); //standard input stream
		String x = scan.next();
		return x;
	}
	public void tsanf(File file, int numMag) {
		// TODO Auto-generated method stub
		
	}
	public void tsanf(File file, String[][] inFileName) {
		// TODO Auto-generated method stub
		
	}
	
	public void floatValue(String inputFile)
	{
		BufferedReader reader = null;

	    try {
	        // use buffered reader to read line by line
	        reader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(inputFile))));

	        float x, y;
	        String line = null;
	        String[] numbers = null;
	        // read line by line till end of file
	        while ((line = reader.readLine()) != null) {
	            // split each line based on regular expression having
	            // "any digit followed by one or more spaces".

	            numbers = line.split("\\d\\s+");

	            x = Float.valueOf(numbers[0].trim());
	           // y = Float.valueOf(numbers[1].trim());

	            System.out.println("x:" + x + " y:" + "y");
	        }
	    } catch (IOException e) {
	        System.err.println("Exception:" + e.toString());
	    } finally {
	        if (reader != null) {
	            try {
	                reader.close();
	            } catch (IOException e) {
	                System.err.println("Exception:" + e.toString());
	            }
	        }
	    }
	}
	public void tsanf(File file, String[][] xray, String[][] ultimate) {
		// TODO Auto-generated method stub
		
	}
	public void tsanf(File fieldFile, float f) {
		// TODO Auto-generated method stub

		
	}
	public float[] readlinebyline() {
		float[] sizerange = new float[28];
		try  
		{  
		File file=new File("SIZE.PRN");    //creates a new file instance
		FileReader fr=new FileReader(file);   //reads the file
		BufferedReader br=new BufferedReader(fr);  //creates a buffering character input stream
		StringBuffer sb=new StringBuffer();    //constructs a string buffer with no characters
		String line;
		int i = 1;
		sizerange[0] = 0;
		while((line=br.readLine())!=null)  
		{


			sizerange[i]= Float.valueOf(line);
			System.out.println(sizerange[i]);
			i++;
		}
		return sizerange;
		//fr.close();    //closes the stream and release the resources
//		System.out.println("Contents of File: ");
//		System.out.println(sb.toString());   //returns a string that textually represents the object

		}  
		catch(IOException e)
		{  
		e.printStackTrace();  
		}
		return sizerange;
	}


	public File tprintf(String fieldFile, float v, float v1) {
		File file = new File(fieldFile);


		try {

			FileWriter myWriter = new FileWriter(fieldFile, true);
			myWriter.write(v + "  " + v1);
			myWriter.write("\n");
			myWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}


		return file;
	}

	public void tprintf(String trans, int numMag, String magAll, String xray, String ultimate) {
		File file = new File(trans);
		try {
			if (file.createNewFile()) {
				System.out.println("File created: " + file.getName());
			} else {
				//System.out.println("File already exists.");
			}
			FileWriter myWriter = new FileWriter(trans, true);

			myWriter.write(numMag+ " ");
			myWriter.write(magAll + " ");
			//myWriter.write(magAll + " ");

			myWriter.write(xray + " " + ultimate);
			myWriter.write("\n");
			myWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}


	}

	public Float readDatafor2File() {
		float data_i= 0;
		try {
			File myObj = new File("DATAFOR2.PRN");
			Scanner myReader = new Scanner(myObj);
							data_i = myReader.nextFloat();
				return data_i;

			//myReader.close();
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}
		return data_i;
	}

	public Float getfloatValue (String fileName, Float data ){
		try {
			File myObj = new File(fileName);
			Scanner myReader = new Scanner(myObj);
			while (myReader.hasNextLine()) {
				String data_i = myReader.nextLine();
				return data;
			}
			myReader.close();
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}
		return data;	}


	public void tprintfcoalese(File file, String coalese) {
		try {

			FileWriter myWriter = new FileWriter(file, true);
			myWriter.write(coalese );
			myWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}


	}

	public void tprintfcheck(String s, String valueOf) {
		try {

			FileWriter myWriter = new FileWriter(s, true);

			myWriter.write(valueOf);
			myWriter.write("\n");
			myWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

    public float surfAreaSphere(float radius) {
		return (float) (4 * 3.14 * Math.pow(radius, 2));

	}



	public float volumeSphere(float r) {
		return (float) (4*22*r*r*r)/(3*7);
	}

	public void tprintfn(String s, String s1) {
		File file = new File(s);
		try {
			if (file.createNewFile()) {
				System.out.println("File created: " + file.getName());
			} else {
				//System.out.println("File already exists.");
			}
			FileWriter myWriter = new FileWriter(s, true);

			myWriter.write(s1+ " ");

			myWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void easierToreadforatran7(String s, String s1) {
		File file = new File(s);
		try {
			if (file.createNewFile()) {
				System.out.println("File created: " + file.getName());
			} else {
				//System.out.println("File already exists.");
			}
			FileWriter myWriter = new FileWriter(s, true);

			myWriter.write(s1+ " ");
			myWriter.write("\n");

			myWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public float crossAreaSphere(float rad) {
		return (float) (3.14 * Math.pow(rad, 2));
	}
}
