package com.ccsem.common;
/*Compound.cpp
 * Description: Mineral classification requirements, used in atran1 and 5
 *
 * Author: Original by Tom Erickson, c version written by Shaker.
 * Modifications:Memory setup is removed as for java it is not required
 * Re-Modified by Binisha and copied in Java format
 *
 * Modification  2020-updated based on the elements

 *  Company: Energy & Environmental Research Center
 * */
public class Compound {

	public boolean quartz(float al, float si) {
		if(si >= 80.0 && al <= 5.0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * IronOxide
	 * @return
	 */
	public boolean ironOxide(float al, float fe, float mg, float s, float si) {
		if(fe >= 80.0 && si < 10.0 && s <= 5.0 && mg <= 5.0 && al <= 5.0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Periclase
	 */
	public  boolean periclase(float ca, float mg) {
		if(mg >= 80.0 && ca <= 5.0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Rutile
	 */
	public  boolean rutile(float ba, float s, float ti) {
		if(ba + ti >= 80.0 && s <= 5.0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * AluminumOxide
	 */
	public  boolean aluminumOxide(float al) {
		if(al >= 80.0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Calcite
	 * @return
	 */
	public boolean calcite(float al, float ba, float ca, float mg, float p, float s, float si, float ti) {
		if(ca >= 80.0 && s < 10.0 && mg <= 5.0 && si <= 5.0 && p <= 5.0 && ti <= 5.0 && ba <= 5.0 && al <= 5.0) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Dolomite
	 * @return
	 */
	public boolean dolomite(float mg, float ca) {
		if(mg + ca >= 80.0 && mg > 5.0 && ca > 10.0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Ankerite
	 */
	public  boolean ankerite(float ca, float fe, float mg, float s) {
		if(ca + fe + mg >= 80.0 && ca > 20.0 && s > 15.0 && fe > 20.0 && ca < fe) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Kaolinite
	 * @return
	 */
	public boolean kaolinite(float al, float ca, float fe, float k, float na, float si) {
		if(al < 0.0001) {
			al = 0.001f;
		}
		if(si + al >= 80.0 && 0.8 < si / al && si / al < 1.5 && k <= 5.0 && ca <= 5.0 && fe <= 5.0 && na <= 5.0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Montmorillonite
	 */
	public  boolean montmorillonite(float al, float ca, float fe, float k, float na, float si) {
		if(si + al >= 80.0 && 01.3 < si / al && si / al < 1.8 && k <= 5.0 && ca <= 5.0 && fe > 5.0 && na > 5.0) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * KAlsilicate
	 */
	public  boolean kAlSilicate(float al, float ca, float fe, float k, float na, float si) {
		if(al + si + k >= 80.0 && k <= 5.0 && si <= 20.0 && al >= 15.0 && ca <= 5.0 && fe <= 5.0 && na <= 5.0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * FEAlsilicate
	 */
	public  boolean feAlSilicate(float al, float ca, float fe, float k, float na, float s, float si) {
		if(al + si + fe >= 80.0 && fe <= 5.0 && si <= 20.0 && al >= 15.0 && s <= 5.0 && ca <= 5.0 && k <= 5.0 && na <= 5.0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * CaAlsilicate
	 */
	public  boolean caAlSilicate(float al, float ca, float fe, float k, float na, float s, float si) {
		if(al + si + ca >= 80.0 && ca > 5.0 && si > 20.0 && al >= 15.0 && na <= 5.0 && s <= 5.0 && k <= 5.0 && fe <= 5.0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * NaAlsilicate
	 */
	public  boolean naAlSilicate(float al, float ca, float fe, float k, float na, float s, float si) {
		if(na + al + si >= 80.0 && na > 5.0 && s <= 5.0 && k <= 5.0 && ca <= 5.0 && fe <= 5.0 && al >= 15.0 && si > 20.0) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Aluminosilicate
	 */
	public  boolean aluminoSilicate(float al, float ca, float fe, float k, float na, float si) {
		if(al + si >= 80.0 && si > 5.0 && si > 20.0 && al > 20.0 && na <= 5.0 && k <= 5.0 && ca <= 5.0 && fe <= 5.0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * MixedAlsilicate
	 */
	public  boolean mixedAlSilicate(float al, float ca, float fe, float k, float na, float s, float si) {
		if(na + al + si + k + ca + fe >= 80.0 && na < 10.0 && al > 20.0 && si > 20.0 && k < 10.0 && ca <= 10.0 && fe < 10.0 && s <= 5.0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * FeSilicate
	 */
	public  boolean feSilicate(float al, float ca, float fe, float k, float na, float s, float si) {
		if(si + fe >= 80.0 && na <= 5.0 && s <= 5.0 && al >= 5.0 && k <= 5.0 && ca <= 5.0 && si > 20.0 && fe <= 10.0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * CaSilicate
	 */
	public  boolean caSilicate(float al, float ca, float fe, float k, float na, float s, float si) {
		if(ca + si >= 80.0 && na <= 5.0 && al <= 5.0 && s <= 5.0 && k <= 5.0 && fe <= 5.0 && ca > 10.0 && si > 20.0) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * CaAluminate
	 */
	public  boolean caAluminate(float al, float ca, float p, float s, float si) {
		if(ca + al >= 80.0 && si <= 5.0 && s <= 5.0 && p <= 5.0 && al > 15.0 && ca > 20.0) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Pyrite
	 * @return
	 */
	public boolean pyrite(float ba, float ca, float fe, float s) {
		if(fe + s >= 80.0 && fe >= 15.0 && fe / s <= 0.7 && s > 40.0 && ca < 10.0 && ba < 5.0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Pyrrhotiite
	 * @return
	 */
	public boolean pyrrhotiite(float ba, float ca, float fe, float s) {
		if(fe + s >= 80.0 && fe >= 20.0 && fe / s > 0.7 && fe / s < 1.5 && s > 20 && ca < 10.0 && ba < 5.0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * OxidizedPyrrhotiite
	 */
	public  boolean oxidizedPyrrhotiite(float ba, float ca, float fe, float s) {
		if(fe + s >= 80.0 && fe > 40.0 && fe / s <= 2.0 && s > 5.0 && ca < 10.0 && ba < 5.0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Gypsum
	 * @return
	 */
	public boolean gypsum(float ba, float ca, float fe, float s, float si, float ti) {
		if(ca + s >= 80.0 && si < 10.0 && s > 20.0 && ca > 20.0 && ba < 10.0 && ti < 10.0) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Barite
	 * @return
	 */
	public boolean barite(float ba, float ca, float fe, float s, float ti) {
		if(ba + s + ti >= 80.0 && fe < 10.0 && s > 20.0 && ca <= 5.0 && ba + ti > 20.0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Apatite
	 */
	public  boolean apatite(float al, float ca, float p, float s) {
		if(ca + p >= 80.0 && p >= 20.0 && ca >= 20.0 && al <= 5.0 && s <= 5.0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * CaAlPhosphorus
	 */
	public  boolean caAlPhosphorus(float al, float ca, float p, float s, float si) {
		if(al + p + ca >= 80.0 && al > 10.0 && p > 10.0 && ca > 10.0 && s <= 5.0 && si <= 5.0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * KCL
	 */
	public  boolean kcl(float cl, float k) {
		if(cl + k >= 80.0 && cl >= 30.0 && k >= 30.0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Gypsumbarite
	 */
	public  boolean gypsumbarite(float ba, float ca, float fe, float s, float ti) {
		if(ca + ba + s + ti >= 80.0 && fe < 5.0 && s > 20.0 && ca <= 5.0 && ba > 5.0 && ti > 5.0) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * GypsumAlSi
	 */
	public  boolean	gypsumAlSi(float al, float ca, float s, float si) {
		if(ca + al + s + si >= 80.0 && ca > 5.0 && al > 5.0 && si > 5.0 && s > 5.0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * SiRich
	 */
	public  boolean siRich(float si) {
		if(si >= 65.0 && si < 80.0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * CaRich
	 */
	public  boolean caRich(float al, float ca) {
		if(ca >= 65.0 && ca < 80.0 && al < 15.0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * CaSiRich
	 */
	public  boolean caSiRich(float ca, float si) {
		if(si + ca >= 80.0 && ca >= 20.0 && si >= 20.0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * IronCarbonate
	 * @return
	 */
	public boolean ironCarbonate(float fe, float s) {
		if(fe > 65.0 && s <= 5.0) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * OxPyrrhotiite
	 * @return
	 */
	public boolean oxPyrrhotiite(float ba, float ca, float fe, float s) {
		if(fe > 40.0 && fe / s >=1.5 && s > 5.0 && ca < 10.0 && ba < 5.0 && fe + s > 80.0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * CaAlP
	 * @return
	 */
	public boolean caAlP(float al, float ca, float p, float s, float si) {
		if(al + p + ca > 80.0 && al > 10.0 && p > 10.0 && ca > 10.0 && s < 5.0 && si < 5.0) {
			return true;
		} else {
			return false;
		}
	}
}


