package com.ccsem;

import com.ccsem.common.AllElements;
import com.ccsem.common.Coalescence;
import com.ccsem.common.FileIO;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Scanner;

public class Atran7 {

    public static Float[][][] arrayThreedim = new Float[4][35][8];
    public static Float[] sizeRange = new Float[8];
    public static Float[] cut2 = new Float[5];
    public static String[] sizeName= new String[7];
    public static String[] phase = new String[35];
    public static String[] talkDiameter = {"AERO. DIAMETER"};
    public static String weightPercent = "WEIGHT PERCENT";
    public static float bogus, subDiameter, percentLoc, percentSub, na, mg, al, si, p, s, cl, volume, weight, k, ca, fe, ba, ti, diameter, shape, phaseSum, phaseSum2, wp1, wp2, wp3, wp4, wp5, wp6, locSize1percent, libSizePercent, subSize1Percent, sizeBin1Total, subMicron1, p1, p2, percent1, percent2, weightDivPhaseSum, phaseSum2Div100, temp1, subMicron2, percentLib, locSize2Percent,
            libSize2Percent, subSize2Percent, sizeBin2Total, subArea, subDensity;
    public static int x, y, size, phaseO, newK;
    public static String[][][] magAll = new String[5][3][60];
    public static AllElements loc, lib, ox, oxide, locSize, libSize, bulkOxide, sizeb, bulk, sub, tempLoc, tempLib, tempSub;
    public static int part, frame, type, index, itisLocFile, XRaycts;
    public static double sum = 0;
    public static File inputFile, truePercentFile, subMicFile, comboAshFile;
    public static String boilerType ;
    public static float boilerValue, totalSub, totalBulkOxide, totalBulk1, totalBulk2, totalBulk3, totalBulk4, totalBulk5, totalBulk6, totalBulk7;
    public static double[] density = {0.00, 2.65, 3.80, 3.61, 4.90, 4.00, 3.30, 3.45, 3.50, 2.23, 2.17, 2.60, 2.80, 2.65, 2.60, 2.65, 2.65, 4.40, 3.09, 2.80, 5.00, 4.60, 5.30, 2.50, 5.72, 3.20, 2.80, 1.99, 3.50, 2.60, 2.65, 2.60, 2.60, 2.70};
    public static FileIO fileIO;
    public static String ultimate ;
    public static String xray, comboashfileName= "COMBOASH.PRN" ;
    //public static String[] magAll ;
    public static ArrayList<AllElements> allElementsSize = new ArrayList<>();
    public static ArrayList<AllElements> libElementsSize = new ArrayList<>();
    public static ArrayList<AllElements> locElementsSize = new ArrayList<>();
    public static ArrayList<AllElements> bulkElementsSize = new ArrayList<>();



    private static DecimalFormat df = new DecimalFormat("0.00");

//    public static ArrayList<LocAshData> locAshDataArrayList = new ArrayList<>();
//    public static LocAshData locAshData;
    public void ProcessAtran7(float[][][] myarray, float percLoc, float peRcSiLoc, String boilerType, Float[][][] arrayThreedims, ArrayList<Coalescence> locAshDataArrayList, ArrayList<Coalescence> libAshDataArrayList, Float[] sizeRang, String[] sizeNam, String[] nameselement) {
        loc = new AllElements();
        lib = new AllElements();
        ox = new AllElements();
        oxide = new AllElements();
        locSize = new AllElements();
        libSize= new AllElements();
        bulkOxide= new AllElements();
        sizeb= new AllElements();
        bulk= new AllElements();
        sub= new AllElements();
        tempLoc= new AllElements();
        tempLib = new AllElements();
        tempSub = new AllElements();
        percentLoc = percLoc/ 100; //% locked minerals
        percentSub =peRcSiLoc/ 100; //%submicron minerals
        /* The percentsub will not change //percentlib = 100 - percentLoc, then must renormalize leaving %sub as original value
         */
        sizeRange = sizeRang;
        sizeName = sizeNam;
        percentLib = 1-percentLoc;
        percentLib *=(1-percentSub); //%liberated particles
        percentLoc *=1-percentSub; //%lacked particles
        if(boilerType==" Fluidized"){
            boilerValue= 15;
        }else if(boilerType=="Pulverized"){
            boilerValue= 15;

        }else if(boilerType=="Low"){
            boilerValue= 15;

        }else if(boilerType=="Cyclone"){
            boilerValue= 10;

        }
        fileIO = new FileIO();
        this.arrayThreedim = arrayThreedims;
        for (int size = 1; size <= 7; size++) {
                for (int phase = 1; phase <= 34; phase++) {
                    arrayThreedim[3][phase][size] = Float.valueOf(0);

                }

        }

        //combining lock and lib for each mineral type from sp.prn
        for (int phase = 1; phase <= 33; phase++) {
            for (int size = 1; size <=7 ; size++) {
                arrayThreedim[3][phase][size] = arrayThreedim[1][phase][size]* percentLoc + (arrayThreedim[2][phase][size]*percentLib);
            }
        }
        for (int size = 1; size <= 7; size++) {
            for (int m = 1; m <=3 ; m++) {
                for (int phase = 1; phase <= 33; phase++) {
                    //Issue here is we don't have phase array 34(Initialized zero at the beginning: Binisha 2020)
                    arrayThreedim[m][34][size] += arrayThreedim[m][phase][size] ;
                }
            }
        }
        /*splitting the submicron ash between 0-1um and 1-3um size bins
         * BoilerType is % of submicron ash that will enter the 1-3um size range*/
        subMicron1 = ((1-boilerValue) * percentSub)* 100;//ash in 0-1um from submicron
        subMicron2 = (percentSub * boilerValue) * 100; //ash in 1-3um from submicron
        sizeBin1Total =arrayThreedim[3][34][1]+ subMicron1;// sizebin 1 total mass

        //composition % for smallest size range, submicron == >1um
        if(sizeBin1Total == 0){// no mass in size range 1-3um
            subSize1Percent =0;
            locSize1percent = 0;
            libSizePercent = 0;
        }else{
            subSize1Percent = subMicron1/sizeBin1Total;
            locSize1percent = (percentLoc* arrayThreedim[1][34][1])/ sizeBin1Total;
            locSize1percent = (percentLoc *arrayThreedim[2][34][1])/ sizeBin1Total;

        }
        /*1-3um size range, size bin 2
         * arrayThreedim[3][34][2] 1-3um*/
        sizeBin2Total = arrayThreedim[3][34][2] + subMicron2; //size bin 2 total mass

        //composition % for 2 bin size ranfe, 1-3 um
        if(sizeBin2Total==0) { // no mass in size range 1-3um
            subSize2Percent= 0;
            locSize2Percent = 0;
            libSize2Percent = 0;
        }else{
            subSize2Percent = subMicron2/sizeBin2Total;
            locSize2Percent = (percentLoc* arrayThreedim[1][34][1])/ sizeBin2Total;
            libSize2Percent = (percentLoc *arrayThreedim[2][34][1])/ sizeBin2Total;

        }

        readSubMicFile("SUBMIC.PRN");
        //Begining of first page printing
        fileIO.tprintf(comboashfileName,"\n\n");

        fileIO.tprintf(comboashfileName,  " ENTRAINED FLY ASH" );
        fileIO.tprintf(comboashfileName,  " PREDICTED FROM ADVANCED ANALYSIS" );
        fileIO.tprintf(comboashfileName, String.format("%1$"+15+ "s", talkDiameter)+ "   "  + String.format("%1$"+5+ "s", sizeName[1] )+"  "+String.format("%1$"+5+ "s", sizeName[2]  )+"  "+String.format("%1$"+5+ "s", sizeName[3]  )+"  "+ String.format("%1$"+5+ "s", sizeName[4] )+"  "+String.format("%1$"+5+ "s", sizeName[5] )+"  "+String.format("%1$"+5+ "s", sizeName[6]  ));
        fileIO.tprintf(comboashfileName,"\n\n");
        for (int i = 1; i <=33; i++) {

            fileIO.tprintfn(comboashfileName, String.format("%1$"+20+ "s", nameselement[i]));

            fileIO.tprintf(comboashfileName, "  " + String.format("%1$"+10+ "s", df.format(arrayThreedim[3][i][1]))+ "  " +String.format("%1$"+10+ "s", df.format(arrayThreedim[3][i][2]))+ "  " + String.format("%1$"+10+ "s", df.format(arrayThreedim[3][i][3]))+ "  " + String.format("%1$"+10+ "s", df.format(arrayThreedim[3][i][4]))+ "  " + String.format("%1$"+10+ "s", df.format(arrayThreedim[3][i][5]))+ "  " + String.format("%1$"+10+ "s", df.format(arrayThreedim[3][i][6]))+ "  " + String.format("%1$"+10+ "s", df.format(arrayThreedim[3][i][7])));

        }
        fileIO.tprintf(comboashfileName,"\n\n\n");
        fileIO.tprintf(comboashfileName,  " NUCLEATED FLY ASH AVERAGE COMPOSITION(OXIDES) \n" );
        //print element labels

        fileIO.tprintf(comboashfileName, "  " + String.format("%1$"+5+ "s", "NA2O")+ "  " +String.format("%1$"+5+ "s", "MGO")+ "  " + String.format("%1$"+5+ "s", "AL2O3")+ "  " + String.format("%1$"+5+ "s", "SIO2")+ "  " + String.format("%1$"+5+ "s", "P2O5")+ "  " + String.format("%1$"+5+ "s", "SO3")+ "  " + String.format("%1$"+5+ "s", "CL2O7")+ "  " + String.format("%1$"+5+ "s", "K20")+ "  " + String.format("%1$"+5+ "s", "CAO")+ "  " + String.format("%1$"+5+ "s", "FE2O3")+ "  " + String.format("%1$"+5+ "s", "BAO")+ "  " + String.format("%1$"+5+ "s", "TIO2"));
        //print elements
        fileIO.tprintf(comboashfileName, "  " + String.format("%1$"+5+ "s", df.format((sub.getNA()* 100/totalSub)))+ "  " +String.format("%1$"+5+ "s", df.format((sub.getMG()* 100/totalSub)))+ "  " + String.format("%1$"+5+ "s", df.format((sub.getAL()* 100/totalSub)))+ "  " + String.format("%1$"+5+ "s", df.format((sub.getSI()* 100/totalSub)))+ "  " + String.format("%1$"+5+ "s", df.format((sub.getP()* 100/totalSub)))+ "  " + String.format("%1$"+5+ "s", df.format((sub.getS()* 100/totalSub)))+ "  " + String.format("%1$"+5+ "s", df.format((sub.getCL()* 100/totalSub)))+ "  " + String.format("%1$"+5+ "s", df.format((sub.getK()* 100/totalSub)))+ "  " + String.format("%1$"+5+ "s", df.format((sub.getCA()* 100/totalSub)))+ "  " + String.format("%1$"+5+ "s", df.format((sub.getFE()* 100/totalSub)))+ "  " + String.format("%1$"+5+ "s", df.format((sub.getBA()* 100/totalSub)))+ "  " + String.format("%1$"+5+ "s", df.format((sub.getTI()* 100/totalSub))));

        //fileIO.tprintf(comboashfileName,  String.format("%1$"+27+ "s", "1.0")+ "    2.2     4.6    10    22      46  " );

        itisLocFile = 1;
        for (int m = 1; m < 3; m++) {
            for (int newK = 0; newK <=6 ; newK++) {
                sizeb= new AllElements();
                sizeb.setSI(0);
                sizeb.setAL(0);
                sizeb.setNA(0);
                sizeb.setMG(0);
                sizeb.setP(0);
                sizeb.setK(0);
                sizeb.setCA(0);
                sizeb.setFE(0);
                sizeb.setCL(0);
                sizeb.setS(0);
                sizeb.setBA(0);
                sizeb.setTI(0);
                allElementsSize.add(sizeb);

            }
            oxide.setSI(0);
            oxide.setAL(0);
            oxide.setNA(0);
            oxide.setMG(0);
            oxide.setP(0);
            oxide.setK(0);
            oxide.setCA(0);
            oxide.setFE(0);
            oxide.setCL(0);
            oxide.setS(0);
            oxide.setBA(0);
            oxide.setTI(0);

            if(m==2){
                locAshDataArrayList = libAshDataArrayList;
            }
            for (int i = 0; i < locAshDataArrayList.size(); i++) {
                weight = (float) (density[locAshDataArrayList.get(i).getPhase()] * locAshDataArrayList.get(i).getPv());
                /*converting elemental analysis to equivalent % oxide
                 * no need to convert TE, they stay as ppm*/
                ox.setSI((float) (locAshDataArrayList.get(i).getSi()/ (100 * 0.4675)));
                ox.setAL((float) (locAshDataArrayList.get(i).getAl()/ (100 * 0.5292)));
                ox.setNA((float) (locAshDataArrayList.get(i).getNA()/ (100 * 0.7420)));
                ox.setMG((float) (locAshDataArrayList.get(i).getMg()/ (100 * 0.6032)));
                ox.setP((float) (locAshDataArrayList.get(i).getP()/ (100 * 0.4364)));
                ox.setK((float) (locAshDataArrayList.get(i).getK()/ (100 * 0.8300)));
                ox.setCA((float) (locAshDataArrayList.get(i).getCa()/ (100 * 0.7156)));
                ox.setFE((float) (locAshDataArrayList.get(i).getFe()/ (100 * 0.6994)));
                ox.setTI((float) (locAshDataArrayList.get(i).getTi()/ (100 * 0.5994)));
                ox.setBA(0);
                ox.setS((float) (locAshDataArrayList.get(i).getS()/ (100 * 0.4005)));
                ox.setCL((float) (locAshDataArrayList.get(i).getCl()/ (100 * 0.6890)));

                phaseSum = (float) (ox.getSI()+ox.getAL()+ox.getNA()+ox.getMG()+ ox.getP()+ ox.getK()+ ox.getCA()+ ox.getFE()+ ox.getTI()+ ox.getBA()+ ox.getS()+ ox.getCL()+ 0.0001);

                //need to add weight because we added oxygen
                weightDivPhaseSum = weight / phaseSum;
//                System.out.println(oxide.getSI()+ " before change");
                oxide.setSI(oxide.getSI()+(weightDivPhaseSum * ox.getSI()));
                oxide.setAL(oxide.getAL()+(weightDivPhaseSum * ox.getAL()));
                oxide.setNA(oxide.getNA()+(weightDivPhaseSum * ox.getNA()));
                oxide.setMG(oxide.getMG()+(weightDivPhaseSum * ox.getMG()));
                oxide.setP(oxide.getP()+(weightDivPhaseSum * ox.getP()));
                oxide.setK(oxide.getK()+(weightDivPhaseSum * ox.getK()));
                oxide.setCA(oxide.getCA()+(weightDivPhaseSum * ox.getCA()));
                oxide.setFE(oxide.getFE()+(weightDivPhaseSum * ox.getFE()));
                oxide.setCL(oxide.getCL()+(weightDivPhaseSum * ox.getCL()));
                oxide.setS(oxide.getS()+(weightDivPhaseSum * ox.getS()));
                oxide.setBA(oxide.getBA()+(weightDivPhaseSum * ox.getBA()));
                oxide.setTI(oxide.getTI()+(weightDivPhaseSum * ox.getTI()));
//                System.out.println(oxide.getSI()+ " oxide.getSI()");
//                System.out.println(weightDivPhaseSum+ " weightDivPhaseSum");
//                System.out.println(ox.getSI()+ " ox.getSI()\n\n");

                //after reading the "name.xls" file , I obtain the sizebins and thus dynamically determine the size placement -- shaker
                index = 6;
                for (int j = 1; j <6 ; j++) {
                    //System.out.println(sizeRange[j]+ "size");
                    if (locAshDataArrayList.get(i).getAvgdiameter() <= sizeRange[j]) {
                        index = j;
                        break;
                    }
                }


                allElementsSize.get(index).setSI(allElementsSize.get(index).getSI() + (weightDivPhaseSum * ox.getSI()));
                allElementsSize.get(index).setAL(allElementsSize.get(index).getAL() + (weightDivPhaseSum * ox.getAL()));
                allElementsSize.get(index).setNA(allElementsSize.get(index).getNA() + (weightDivPhaseSum * ox.getNA()));
                allElementsSize.get(index).setMG(allElementsSize.get(index).getMG() + (weightDivPhaseSum * ox.getMG()));
                allElementsSize.get(index).setP(allElementsSize.get(index).getP() + (weightDivPhaseSum * ox.getP()));
                allElementsSize.get(index).setK(allElementsSize.get(index).getK() + (weightDivPhaseSum * ox.getK()));
                allElementsSize.get(index).setCA(allElementsSize.get(index).getCA() + (weightDivPhaseSum * ox.getCA()));
                allElementsSize.get(index).setFE(allElementsSize.get(index).getFE() + (weightDivPhaseSum * ox.getFE()));
                allElementsSize.get(index).setCL(allElementsSize.get(index).getCL() + (weightDivPhaseSum * ox.getCL()));
                allElementsSize.get(index).setS(allElementsSize.get(index).getS() + (weightDivPhaseSum * ox.getS()));
                allElementsSize.get(index).setBA(allElementsSize.get(index).getBA() + (weightDivPhaseSum * ox.getBA()));
                allElementsSize.get(index).setTI(allElementsSize.get(index).getTI() + (weightDivPhaseSum * ox.getTI()));


            }//end for loop i(feof file)
            // calculation of total in Locasho.xls
            phaseSum2 = (float) (oxide.getSI()+ oxide.getAL()+ oxide.getNA()+ oxide.getMG()+ oxide.getP()+ oxide.getK()+ oxide.getCA()+ oxide.getFE()+ oxide.getTI()+ oxide.getBA()+ oxide.getS()+ oxide.getCL()+ 0.0001);

            phaseSum2Div100 = phaseSum2 /100;
            if(m == 1){
                //elements.c produces average loc oxide percentage
                loc = divlocOxideElements( oxide, phaseSum2Div100);

                for (int i = 0; i <=6 ; i++) {
                    sum = findElementTotal(allElementsSize.get(i));


                    if (sum < 0.00000001)
                        sum = 0.00000001;
                    //produces amount in each size of each oxide LocSize[I].x
                    // For the Div Elemensts we assume to be division of phasesum2div100 with size

                    locSize = new AllElements();
                    locSize.setTI((float) (allElementsSize.get(i).getTI()/(sum/100)));
                    locSize.setSI((float) (allElementsSize.get(i).getSI()/(sum/100)));
                    locSize.setAL((float) (allElementsSize.get(i).getAL()/(sum/100)));
                    locSize.setNA((float) (allElementsSize.get(i).getNA()/(sum/100)));
                    locSize.setMG((float) (allElementsSize.get(i).getMG()/(sum/100)));
                    locSize.setP((float) (allElementsSize.get(i).getP()/(sum/100)));
                    locSize.setK((float) (allElementsSize.get(i).getK()/(sum/100)));
                    locSize.setCA((float) (allElementsSize.get(i).getCA()/(sum/100)));
                    locSize.setFE((float) (allElementsSize.get(i).getFE()/(sum/100)));
                    locSize.setCL((float) (allElementsSize.get(i).getCL()/(sum/100)));
                    locSize.setS((float) (allElementsSize.get(i).getS()/(sum/100)));
                    locSize.setBA((float) (allElementsSize.get(i).getBA()/(sum/100)));
                    locElementsSize.add(locSize);


                }
            }else{
                lib = divlibOxideElements( oxide, phaseSum2Div100);

                for (int i = 0; i <=6 ; i++) {
                    sum = findElementTotal(allElementsSize.get(i));


                    if (sum < 0.00000001)
                        sum = 0.00000001;
                    //produces amount in each size of each oxide LocSize[I].x
                    // For the Div Elemensts we assume to be division of phasesum2div100 with size
                    libSize = new AllElements();
                    libSize.setTI((float) (allElementsSize.get(i).getTI()/(sum/100)));
                    libSize.setSI((float) (allElementsSize.get(i).getSI()/(sum/100)));
                    libSize.setAL((float) (allElementsSize.get(i).getAL()/(sum/100)));
                    libSize.setNA((float) (allElementsSize.get(i).getNA()/(sum/100)));
                    libSize.setMG((float) (allElementsSize.get(i).getMG()/(sum/100)));
                    libSize.setP((float) (allElementsSize.get(i).getP()/(sum/100)));
                    libSize.setK((float) (allElementsSize.get(i).getK()/(sum/100)));
                    libSize.setCA((float) (allElementsSize.get(i).getCA()/(sum/100)));
                    libSize.setFE((float) (allElementsSize.get(i).getFE()/(sum/100)));
                    libSize.setCL((float) (allElementsSize.get(i).getCL()/(sum/100)));
                    libSize.setS((float) (allElementsSize.get(i).getS()/(sum/100)));
                    libSize.setBA((float) (allElementsSize.get(i).getBA()/(sum/100)));
                    libElementsSize.add(libSize);
                }

            }//if(itistheLocFIle)

        }    //m loop ends

        //Bulk oxide composition
        tempLoc = multElementsLoc( loc, percentLoc);
        tempSub = multElementsSub( sub, percentSub);
        tempLib = multElementsLib( lib, percentLib);

        zeroElements();
        addTwoElemnts( tempLoc);
        addTwoElemnts( tempSub);
        addTwoElemnts( tempLib);

        //smallest size range, size bin 1, combining loc lib and submicron particles
        tempLoc = multElementsLoc( locElementsSize.get(1), locSize1percent);
        tempSub = multElementsSub( sub, subSize1Percent);
        tempLib = multElementsLib( libElementsSize.get(1), libSizePercent);

        zeroElementsbulk();
        addTwoElemntsBulk1( );


        //smallest size range, size bin 2, combining loc lib and submicron particles
        tempLoc = multElementsLoc( locElementsSize.get(2), locSize2Percent);
        tempSub = multElementsSub( sub, subSize2Percent);
        tempLib = multElementsLib( libElementsSize.get(2), libSize2Percent);

        addTwoElemntsBulk2( );


        //Remaining size ranges, combining loc and lib particles

        for (int i = 3; i <=6 ; i++) {
            p1 = arrayThreedim[1][34][i] * percentLoc; //locked
            p2 = arrayThreedim[2][34][i] * percentLib; //liberated
            System.out.println(p1+ "locElementsSize"+ p2);
            percent1 = (float) (p1/(p1+p2+ 0.000000001));
            percent2 = (float) (p2/ (p1+ p2+ 0.000000001));
            tempLoc = multElementsLoc( locElementsSize.get(i), percent1);
            tempLib = multElementsLib( libElementsSize.get(i), percent2);
            bulkElementsSize.get(i).setSI( bulkElementsSize.get(i).getSI()+ tempLoc.getSI());
            bulkElementsSize.get(i).setAL(bulkElementsSize.get(i).getAL()+ tempLoc.getAL());
            bulkElementsSize.get(i).setNA(bulkElementsSize.get(i).getNA()+ tempLoc.getNA());
            bulkElementsSize.get(i).setMG(bulkElementsSize.get(i).getMG()+ tempLoc.getMG());
            bulkElementsSize.get(i).setP(bulkElementsSize.get(i).getP()+ tempLoc.getP());
            bulkElementsSize.get(i).setK(bulkElementsSize.get(i).getK()+ tempLoc.getK());
            bulkElementsSize.get(i).setCA(bulkElementsSize.get(i).getCA()+ tempLoc.getCA());
            bulkElementsSize.get(i).setFE(bulkElementsSize.get(i).getFE()+ tempLoc.getFE());
            bulkElementsSize.get(i).setCL(bulkElementsSize.get(i).getCL()+ tempLoc.getCL());
            bulkElementsSize.get(i).setS(bulkElementsSize.get(i).getS()+ tempLoc.getS());
            bulkElementsSize.get(i).setBA(bulkElementsSize.get(i).getBA()+ tempLoc.getBA());
            bulkElementsSize.get(i).setTI(bulkElementsSize.get(i).getTI()+ tempLoc.getTI());

            bulkElementsSize.get(i).setSI( bulkElementsSize.get(i).getSI()+ tempLib.getSI());
            bulkElementsSize.get(i).setAL(bulkElementsSize.get(i).getAL()+ tempLib.getAL());
            bulkElementsSize.get(i).setNA(bulkElementsSize.get(i).getNA()+ tempLib.getNA());
            bulkElementsSize.get(i).setMG(bulkElementsSize.get(i).getMG()+ tempLib.getMG());
            bulkElementsSize.get(i).setP(bulkElementsSize.get(i).getP()+ tempLib.getP());
            bulkElementsSize.get(i).setK(bulkElementsSize.get(i).getK()+ tempLib.getK());
            bulkElementsSize.get(i).setCA(bulkElementsSize.get(i).getCA()+ tempLib.getCA());
            bulkElementsSize.get(i).setFE(bulkElementsSize.get(i).getFE()+ tempLib.getFE());
            bulkElementsSize.get(i).setCL(bulkElementsSize.get(i).getCL()+ tempLib.getCL());
            bulkElementsSize.get(i).setS(bulkElementsSize.get(i).getS()+ tempLib.getS());
            bulkElementsSize.get(i).setBA(bulkElementsSize.get(i).getBA()+ tempLib.getBA());
            bulkElementsSize.get(i).setTI(bulkElementsSize.get(i).getTI()+ tempLib.getTI());

//            normaliing error, 5/12/94 SA

        }
        fileIO.tprintf(comboashfileName,  " \n\n BULK COMPOSITION OF FLYASH AS OXIDES\n" );

        //print element labels

        fileIO.tprintf(comboashfileName, "  " + String.format("%1$"+5+ "s", "NA2O")+ "  " +String.format("%1$"+5+ "s", "MGO")+ "  " + String.format("%1$"+5+ "s", "AL2O3")+ "  " + String.format("%1$"+5+ "s", "SIO2")+ "  " + String.format("%1$"+5+ "s", "P2O5")+ "  " + String.format("%1$"+5+ "s", "SO3")+ "  " + String.format("%1$"+5+ "s", "CL2O7")+ "  " + String.format("%1$"+5+ "s", "K20")+ "  " + String.format("%1$"+5+ "s", "CAO")+ "  " + String.format("%1$"+5+ "s", "FE2O3")+ "  " + String.format("%1$"+5+ "s", "BAO")+ "  " + String.format("%1$"+5+ "s", "TIO2"));
        //print elements
        fileIO.tprintf(comboashfileName, "  " + String.format("%1$"+5+ "s", df.format((bulkOxide.getNA()* 100/totalBulkOxide)))+ "  " +String.format("%1$"+5+ "s",df.format(( bulkOxide.getMG()* 100/totalBulkOxide)))+ "  " + String.format("%1$"+5+ "s",df.format((bulkOxide.getAL()* 100/totalBulkOxide)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkOxide.getSI()* 100/totalBulkOxide)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkOxide.getP()* 100/totalBulkOxide)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkOxide.getS()* 100/totalBulkOxide)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkOxide.getCL()* 100/totalBulkOxide)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkOxide.getCL()* 100/totalBulkOxide)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkOxide.getK()* 100/totalBulkOxide)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkOxide.getCA()* 100/totalBulkOxide)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkOxide.getFE()* 100/totalBulkOxide)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkOxide.getBA()* 100/totalBulkOxide)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkOxide.getTI()* 100/totalBulkOxide))));



        wp1 = sizeBin1Total;
        wp2 = sizeBin2Total;
        wp3 = arrayThreedim[3][34][3];
        wp4 = arrayThreedim[3][34][4];
        wp5 = arrayThreedim[3][34][5];
        wp6 = arrayThreedim[3][34][6];

        //Begining of second page printing

        fileIO.tprintf(comboashfileName,  "\n\n\n\n" );
        fileIO.tprintf(comboashfileName,  " BULK COMPOSITION OF < " + sizeRange[1]+ " MICRON ASH" );
        fileIO.tprintf(comboashfileName, String.format("%1$"+20+ "s", weightPercent)+ " "+ wp1);
        //print element labels

        fileIO.tprintf(comboashfileName, "  " + String.format("%1$"+5+ "s", "NA2O")+ "  " +String.format("%1$"+5+ "s", "MGO")+ "  " + String.format("%1$"+5+ "s", "AL2O3")+ "  " + String.format("%1$"+5+ "s", "SIO2")+ "  " + String.format("%1$"+5+ "s", "P2O5")+ "  " + String.format("%1$"+5+ "s", "SO3")+ "  " + String.format("%1$"+5+ "s", "CL2O7")+ "  " + String.format("%1$"+5+ "s", "K20")+ "  " + String.format("%1$"+5+ "s", "CAO")+ "  " + String.format("%1$"+5+ "s", "FE2O3")+ "  " + String.format("%1$"+5+ "s", "BAO")+ "  " + String.format("%1$"+5+ "s", "TIO2"));
        //print elements
        totalBulk1 = bulkElementsSize.get(1).getNA()+ bulkElementsSize.get(1).getMG()+ bulkElementsSize.get(1).getAL()+bulkElementsSize.get(1).getSI()+ bulkElementsSize.get(1).getP()+ bulkElementsSize.get(1).getCL()+ bulkElementsSize.get(1).getK()+ bulkElementsSize.get(1).getCA()+ bulkElementsSize.get(1).getFE()+ bulkElementsSize.get(1).getBA()+ bulkElementsSize.get(1).getTI();
        fileIO.tprintf(comboashfileName, "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(1).getNA()* 100/totalBulk1)))+ "  " +String.format("%1$"+5+ "s",df.format( (bulkElementsSize.get(1).getMG()* 100/totalBulk1)))+ "  " + String.format("%1$"+5+ "s",df.format(( bulkElementsSize.get(1).getAL()* 100/totalBulk1)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(1).getSI()* 100/totalBulk1)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(1).getP()* 100/totalBulk1)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(1).getP()* 100/totalBulk1)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(1).getCL()* 100/totalBulk1)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(1).getK()* 100/totalBulk1)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(1).getCA()* 100/totalBulk1)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(1).getFE()* 100/totalBulk1)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(1).getBA()* 100/totalBulk1)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(1).getTI()* 100/totalBulk1))));

        fileIO.tprintf(comboashfileName,  "\n\n\n\n" );
        fileIO.tprintf(comboashfileName,  " BULK COMPOSITION OF " + sizeName[2]+ " MICRON ASH" );
        fileIO.tprintf(comboashfileName, String.format("%1$"+20+ "s", weightPercent)+ " "+ wp2);

        //print element labels
        fileIO.tprintf(comboashfileName, "  " + String.format("%1$"+5+ "s", "NA2O")+ "  " +String.format("%1$"+5+ "s", "MGO")+ "  " + String.format("%1$"+5+ "s", "AL2O3")+ "  " + String.format("%1$"+5+ "s", "SIO2")+ "  " + String.format("%1$"+5+ "s", "P2O5")+ "  " + String.format("%1$"+5+ "s", "SO3")+ "  " + String.format("%1$"+5+ "s", "CL2O7")+ "  " + String.format("%1$"+5+ "s", "K20")+ "  " + String.format("%1$"+5+ "s", "CAO")+ "  " + String.format("%1$"+5+ "s", "FE2O3")+ "  " + String.format("%1$"+5+ "s", "BAO")+ "  " + String.format("%1$"+5+ "s", "TIO2"));
        //print elements
        totalBulk2 = bulkElementsSize.get(2).getNA()+ bulkElementsSize.get(2).getMG()+ bulkElementsSize.get(2).getAL()+bulkElementsSize.get(2).getSI()+ bulkElementsSize.get(2).getP()+ bulkElementsSize.get(2).getCL()+ bulkElementsSize.get(1).getK()+ bulkElementsSize.get(2).getCA()+ bulkElementsSize.get(2).getFE()+ bulkElementsSize.get(1).getBA()+ bulkElementsSize.get(2).getTI();
        fileIO.tprintf(comboashfileName, "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(2).getNA()* 100/totalBulk2)))+ "  " +String.format("%1$"+5+ "s",df.format( (bulkElementsSize.get(2).getMG()* 100/totalBulk2)))+ "  " + String.format("%1$"+5+ "s",df.format(( bulkElementsSize.get(2).getAL()* 100/totalBulk2)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(2).getSI()* 100/totalBulk2)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(2).getP()* 100/totalBulk2)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(2).getS()* 100/totalBulk2)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(2).getCL()* 100/totalBulk2)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(2).getK()* 100/totalBulk2)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(2).getCA()* 100/totalBulk2)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(2).getFE()* 100/totalBulk2)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(2).getBA()* 100/totalBulk2)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(2).getTI()* 100/totalBulk2))));

        fileIO.tprintf(comboashfileName,  "\n\n\n\n" );
        fileIO.tprintf(comboashfileName,  " BULK COMPOSITION OF " + sizeName[3]+ " MICRON ASH" );
        fileIO.tprintf(comboashfileName, String.format("%1$"+20+ "s", weightPercent)+ " "+ wp3);

        //print element labels
        fileIO.tprintf(comboashfileName, "  " + String.format("%1$"+5+ "s", "NA2O")+ "  " +String.format("%1$"+5+ "s", "MGO")+ "  " + String.format("%1$"+5+ "s", "AL2O3")+ "  " + String.format("%1$"+5+ "s", "SIO2")+ "  " + String.format("%1$"+5+ "s", "P2O5")+ "  " + String.format("%1$"+5+ "s", "SO3")+ "  " + String.format("%1$"+5+ "s", "CL2O7")+ "  " + String.format("%1$"+5+ "s", "K20")+ "  " + String.format("%1$"+5+ "s", "CAO")+ "  " + String.format("%1$"+5+ "s", "FE2O3")+ "  " + String.format("%1$"+5+ "s", "BAO")+ "  " + String.format("%1$"+5+ "s", "TIO2"));
        //print elements
        totalBulk3 = bulkElementsSize.get(3).getNA()+ bulkElementsSize.get(3).getMG()+ bulkElementsSize.get(3).getAL()+bulkElementsSize.get(3).getSI()+ bulkElementsSize.get(3).getP()+ bulkElementsSize.get(3).getCL()+ bulkElementsSize.get(1).getK()+ bulkElementsSize.get(3).getCA()+ bulkElementsSize.get(3).getFE()+ bulkElementsSize.get(1).getBA()+ bulkElementsSize.get(3).getTI();
        fileIO.tprintf(comboashfileName, "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(3).getNA()* 100/totalBulk3)))+ "  " +String.format("%1$"+5+ "s",df.format( (bulkElementsSize.get(3).getMG()* 100/totalBulk3)))+ "  " + String.format("%1$"+5+ "s",df.format(( bulkElementsSize.get(3).getAL()* 100/totalBulk3)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(3).getSI()* 100/totalBulk3)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(3).getP()* 100/totalBulk3)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(3).getS()* 100/totalBulk3)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(3).getCL()* 100/totalBulk3)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(3).getK()* 100/totalBulk3)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(3).getCA()* 100/totalBulk3)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(3).getFE()* 100/totalBulk3)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(3).getBA()* 100/totalBulk3)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(3).getTI()* 100/totalBulk3))));

        fileIO.tprintf(comboashfileName,  "\n\n\n\n" );
        fileIO.tprintf(comboashfileName,  " BULK COMPOSITION OF  " + sizeName[4]+ " MICRON ASH" );
        fileIO.tprintf(comboashfileName, String.format("%1$"+20+ "s", weightPercent)+ " "+ wp4);

        //print element labels
        fileIO.tprintf(comboashfileName, "  " + String.format("%1$"+5+ "s", "NA2O")+ "  " +String.format("%1$"+5+ "s", "MGO")+ "  " + String.format("%1$"+5+ "s", "AL2O3")+ "  " + String.format("%1$"+5+ "s", "SIO2")+ "  " + String.format("%1$"+5+ "s", "P2O5")+ "  " + String.format("%1$"+5+ "s", "SO3")+ "  " + String.format("%1$"+5+ "s", "CL2O7")+ "  " + String.format("%1$"+5+ "s", "K20")+ "  " + String.format("%1$"+5+ "s", "CAO")+ "  " + String.format("%1$"+5+ "s", "FE2O3")+ "  " + String.format("%1$"+5+ "s", "BAO")+ "  " + String.format("%1$"+5+ "s", "TIO2"));
        //print elements
        totalBulk4 = bulkElementsSize.get(4).getNA()+ bulkElementsSize.get(4).getMG()+ bulkElementsSize.get(4).getAL()+bulkElementsSize.get(4).getSI()+ bulkElementsSize.get(4).getP()+ bulkElementsSize.get(4).getCL()+ bulkElementsSize.get(1).getK()+ bulkElementsSize.get(4).getCA()+ bulkElementsSize.get(4).getFE()+ bulkElementsSize.get(1).getBA()+ bulkElementsSize.get(4).getTI();
        fileIO.tprintf(comboashfileName, "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(4).getNA()* 100/totalBulk4)))+ "  " +String.format("%1$"+5+ "s",df.format( (bulkElementsSize.get(4).getMG()* 100/totalBulk4)))+ "  " + String.format("%1$"+5+ "s",df.format(( bulkElementsSize.get(4).getAL()* 100/totalBulk4)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(4).getSI()* 100/totalBulk4)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(4).getP()* 100/totalBulk4)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(4).getS()* 100/totalBulk4)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(4).getCL()* 100/totalBulk4)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(4).getK()* 100/totalBulk4)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(4).getCA()* 100/totalBulk4)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(4).getFE()* 100/totalBulk4)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(4).getBA()* 100/totalBulk4)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(4).getTI()* 100/totalBulk4))));

        fileIO.tprintf(comboashfileName,  "\n\n\n\n" );
        fileIO.tprintf(comboashfileName,  " BULK COMPOSITION OF  " + sizeName[5]+ " MICRON ASH" );
        fileIO.tprintf(comboashfileName, String.format("%1$"+20+ "s", weightPercent)+ " "+ wp5);

        //print element labels
        fileIO.tprintf(comboashfileName, "  " + String.format("%1$"+5+ "s", "NA2O")+ "  " +String.format("%1$"+5+ "s", "MGO")+ "  " + String.format("%1$"+5+ "s", "AL2O3")+ "  " + String.format("%1$"+5+ "s", "SIO2")+ "  " + String.format("%1$"+5+ "s", "P2O5")+ "  " + String.format("%1$"+5+ "s", "SO3")+ "  " + String.format("%1$"+5+ "s", "CL2O7")+ "  " + String.format("%1$"+5+ "s", "K20")+ "  " + String.format("%1$"+5+ "s", "CAO")+ "  " + String.format("%1$"+5+ "s", "FE2O3")+ "  " + String.format("%1$"+5+ "s", "BAO")+ "  " + String.format("%1$"+5+ "s", "TIO2"));
        //print elements
        totalBulk5 = bulkElementsSize.get(5).getNA()+ bulkElementsSize.get(5).getMG()+ bulkElementsSize.get(5).getAL()+bulkElementsSize.get(5).getSI()+ bulkElementsSize.get(5).getP()+ bulkElementsSize.get(5).getCL()+ bulkElementsSize.get(1).getK()+ bulkElementsSize.get(5).getCA()+ bulkElementsSize.get(5).getFE()+ bulkElementsSize.get(1).getBA()+ bulkElementsSize.get(5).getTI();
        fileIO.tprintf(comboashfileName, "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(5).getNA()* 100/totalBulk5)))+ "  " +String.format("%1$"+5+ "s",df.format( (bulkElementsSize.get(5).getMG()* 100/totalBulk5)))+ "  " + String.format("%1$"+5+ "s",df.format(( bulkElementsSize.get(5).getAL()* 100/totalBulk5)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(5).getSI()* 100/totalBulk5)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(5).getP()* 100/totalBulk5)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(5).getS()* 100/totalBulk5)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(5).getCL()* 100/totalBulk5)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(5).getK()* 100/totalBulk5)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(5).getCA()* 100/totalBulk5)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(5).getFE()* 100/totalBulk5)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(5).getBA()* 100/totalBulk5)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(5).getTI()* 100/totalBulk5))));
        fileIO.tprintf(comboashfileName,  "\n\n\n\n" );
        fileIO.tprintf(comboashfileName,  " BULK COMPOSITION OF NOT USED MICRON ASH" );
        fileIO.tprintf(comboashfileName, String.format("%1$"+20+ "s", weightPercent)+ " "+ wp6);
        //print element labels
        fileIO.tprintf(comboashfileName, "  " + String.format("%1$"+5+ "s", "NA2O")+ "  " +String.format("%1$"+5+ "s", "MGO")+ "  " + String.format("%1$"+5+ "s", "AL2O3")+ "  " + String.format("%1$"+5+ "s", "SIO2")+ "  " + String.format("%1$"+5+ "s", "P2O5")+ "  " + String.format("%1$"+5+ "s", "SO3")+ "  " + String.format("%1$"+5+ "s", "CL2O7")+ "  " + String.format("%1$"+5+ "s", "K20")+ "  " + String.format("%1$"+5+ "s", "CAO")+ "  " + String.format("%1$"+5+ "s", "FE2O3")+ "  " + String.format("%1$"+5+ "s", "BAO")+ "  " + String.format("%1$"+5+ "s", "TIO2"));
        //print elements
        totalBulk6 = bulkElementsSize.get(6).getNA()+ bulkElementsSize.get(6).getMG()+ bulkElementsSize.get(6).getAL()+bulkElementsSize.get(6).getSI()+ bulkElementsSize.get(6).getP()+ bulkElementsSize.get(6).getCL()+ bulkElementsSize.get(1).getK()+ bulkElementsSize.get(6).getCA()+ bulkElementsSize.get(6).getFE()+ bulkElementsSize.get(1).getBA()+ bulkElementsSize.get(6).getTI();
        fileIO.tprintf(comboashfileName, "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(6).getNA()* 100/totalBulk6)))+ "  " +String.format("%1$"+5+ "s",df.format( (bulkElementsSize.get(6).getMG()* 100/totalBulk6)))+ "  " + String.format("%1$"+5+ "s",df.format(( bulkElementsSize.get(6).getAL()* 100/totalBulk6)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(6).getSI()* 100/totalBulk6)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(6).getP()* 100/totalBulk6)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(6).getS()* 100/totalBulk6)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(6).getCL()* 100/totalBulk6)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(6).getK()* 100/totalBulk6)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(6).getCA()* 100/totalBulk6)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(6).getFE()* 100/totalBulk6)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(6).getBA()* 100/totalBulk6)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(6).getTI()* 100/totalBulk6))));


        temp1 = 1 - (arrayThreedim[3][34][6]/100 - arrayThreedim[3][34][5]/100);
        cut2[1] = wp1/temp1;
        cut2[2] = wp2/temp1;
        cut2[3] = wp3/temp1;
        cut2[4] = wp4/temp1;
        fileIO.tprintf(comboashfileName,  " \n\n AFTER SIZE SEGREGATION IN BOX\n" );
        fileIO.tprintf(comboashfileName,  sizeName[1] + " MICRON ASH " +cut2[1]);
        fileIO.tprintf(comboashfileName,  sizeName[2] + " MICRON ASH " +cut2[2]);
        fileIO.tprintf(comboashfileName,  sizeName[3] + " MICRON ASH " +cut2[3]);
        fileIO.tprintf(comboashfileName,  cut2[4]+ " MICRON ASH " + sizeRange[3]);
        fileIO.tprintf(comboashfileName,  " \n\n BULK COMPOSITION OF ENTRAINED ASH\n" );

        FindBulkCompositions(bulk, cut2);
        fileIO.tprintf(comboashfileName, "  " + String.format("%1$"+5+ "s", "NA2O")+ "  " +String.format("%1$"+5+ "s", "MGO")+ "  " + String.format("%1$"+5+ "s", "AL2O3")+ "  " + String.format("%1$"+5+ "s", "SIO2")+ "  " + String.format("%1$"+5+ "s", "P2O5")+ "  " + String.format("%1$"+5+ "s", "SO3")+ "  " + String.format("%1$"+5+ "s", "CL2O7")+ "  " + String.format("%1$"+5+ "s", "K20")+ "  " + String.format("%1$"+5+ "s", "CAO")+ "  " + String.format("%1$"+5+ "s", "FE2O3")+ "  " + String.format("%1$"+5+ "s", "BAO")+ "  " + String.format("%1$"+5+ "s", "TIO2"));

        totalBulk7 = bulkElementsSize.get(7).getNA()+ bulkElementsSize.get(7).getMG()+ bulkElementsSize.get(7).getAL()+bulkElementsSize.get(7).getSI()+ bulkElementsSize.get(7).getP()+ bulkElementsSize.get(7).getCL()+ bulkElementsSize.get(1).getK()+ bulkElementsSize.get(7).getCA()+ bulkElementsSize.get(7).getFE()+ bulkElementsSize.get(1).getBA()+ bulkElementsSize.get(7).getTI();
        fileIO.tprintf(comboashfileName, "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(7).getNA()* 100/totalBulk7)))+ "  " +String.format("%1$"+5+ "s",df.format( (bulkElementsSize.get(7).getMG()* 100/totalBulk7)))+ "  " + String.format("%1$"+5+ "s",df.format(( bulkElementsSize.get(7).getAL()* 100/totalBulk7)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(7).getSI()* 100/totalBulk7)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(7).getP()* 100/totalBulk7)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(7).getS()* 100/totalBulk7)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(7).getCL()* 100/totalBulk7)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(7).getK()* 100/totalBulk7)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(7).getCA()* 100/totalBulk7)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(7).getFE()* 100/totalBulk7)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(7).getBA()* 100/totalBulk7)))+ "  " + String.format("%1$"+5+ "s", df.format((bulkElementsSize.get(7).getTI()* 100/totalBulk7))));





    }
    private static void readSubMicFile(String subMicFile) {
        File submicF = new File(subMicFile);
        try {
            Scanner scanner = new Scanner(submicF);
            bogus = Float.valueOf(scanner.next());
            sub.setNA(Float.valueOf(scanner.next()));
            sub.setMG(Float.valueOf(scanner.next()));
            sub.setAL( Float.valueOf(scanner.next()));
            sub.setSI(Float.valueOf(scanner.next()));
            sub.setP(Float.valueOf(scanner.next()));
            sub.setS(Float.valueOf(scanner.next()));
            sub.setCL( Float.valueOf(scanner.next()));
            sub.setK(Float.valueOf(scanner.next()));
            sub.setCA(Float.valueOf(scanner.next()));
            sub.setFE(Float.valueOf(scanner.next()));
            sub.setBA(Float.valueOf(scanner.next()));
            sub.setTI(Float.valueOf(scanner.next()));
            subDiameter= Float.valueOf(scanner.next());
            subArea = Float.valueOf(scanner.next());
            subDensity = Float.valueOf(scanner.next());

            totalSub = sub.getNA()+ sub.getMG()+ sub.getAL()+ sub.getSI()+ sub.getP()+ sub.getS()+ sub.getCL()+ sub.getK()+ sub.getCA()+ sub.getFE()+ sub.getBA()+ sub.getTI();



        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    private static void addTwoElemntsBulk1() {
        bulkElementsSize.get(1).setSI( bulkElementsSize.get(1).getSI()+ tempLoc.getSI());
        bulkElementsSize.get(1).setAL(bulkElementsSize.get(1).getAL()+ tempLoc.getAL());
        bulkElementsSize.get(1).setNA(bulkElementsSize.get(1).getNA()+ tempLoc.getNA());
        bulkElementsSize.get(1).setMG(bulkElementsSize.get(1).getMG()+ tempLoc.getMG());
        bulkElementsSize.get(1).setP(bulkElementsSize.get(1).getP()+ tempLoc.getP());
        bulkElementsSize.get(1).setK(bulkElementsSize.get(1).getK()+ tempLoc.getK());
        bulkElementsSize.get(1).setCA(bulkElementsSize.get(1).getCA()+ tempLoc.getCA());
        bulkElementsSize.get(1).setFE(bulkElementsSize.get(1).getFE()+ tempLoc.getFE());
        bulkElementsSize.get(1).setCL(bulkElementsSize.get(1).getCL()+ tempLoc.getCL());
        bulkElementsSize.get(1).setS(bulkElementsSize.get(1).getS()+ tempLoc.getS());
        bulkElementsSize.get(1).setBA(bulkElementsSize.get(1).getBA()+ tempLoc.getBA());
        bulkElementsSize.get(1).setTI(bulkElementsSize.get(1).getTI()+ tempLoc.getTI());

        bulkElementsSize.get(1).setSI( bulkElementsSize.get(1).getSI()+ tempSub.getSI());
        bulkElementsSize.get(1).setAL(bulkElementsSize.get(1).getAL()+ tempSub.getAL());
        bulkElementsSize.get(1).setNA(bulkElementsSize.get(1).getNA()+ tempSub.getNA());
        bulkElementsSize.get(1).setMG(bulkElementsSize.get(1).getMG()+ tempSub.getMG());
        bulkElementsSize.get(1).setP(bulkElementsSize.get(1).getP()+ tempSub.getP());
        bulkElementsSize.get(1).setK(bulkElementsSize.get(1).getK()+ tempSub.getK());
        bulkElementsSize.get(1).setCA(bulkElementsSize.get(1).getCA()+ tempSub.getCA());
        bulkElementsSize.get(1).setFE(bulkElementsSize.get(1).getFE()+ tempSub.getFE());
        bulkElementsSize.get(1).setCL(bulkElementsSize.get(1).getCL()+ tempSub.getCL());
        bulkElementsSize.get(1).setS(bulkElementsSize.get(1).getS()+ tempSub.getS());
        bulkElementsSize.get(1).setBA(bulkElementsSize.get(1).getBA()+ tempSub.getBA());
        bulkElementsSize.get(1).setTI(bulkElementsSize.get(1).getTI()+ tempSub.getTI());

        bulkElementsSize.get(1).setSI( bulkElementsSize.get(1).getSI()+ tempLib.getSI());
        bulkElementsSize.get(1).setAL(bulkElementsSize.get(1).getAL()+ tempLib.getAL());
        bulkElementsSize.get(1).setNA(bulkElementsSize.get(1).getNA()+ tempLib.getNA());
        bulkElementsSize.get(1).setMG(bulkElementsSize.get(1).getMG()+ tempLib.getMG());
        bulkElementsSize.get(1).setP(bulkElementsSize.get(1).getP()+ tempLib.getP());
        bulkElementsSize.get(1).setK(bulkElementsSize.get(1).getK()+ tempLib.getK());
        bulkElementsSize.get(1).setCA(bulkElementsSize.get(1).getCA()+ tempLib.getCA());
        bulkElementsSize.get(1).setFE(bulkElementsSize.get(1).getFE()+ tempLib.getFE());
        bulkElementsSize.get(1).setCL(bulkElementsSize.get(1).getCL()+ tempLib.getCL());
        bulkElementsSize.get(1).setS(bulkElementsSize.get(1).getS()+ tempLib.getS());
        bulkElementsSize.get(1).setBA(bulkElementsSize.get(1).getBA()+ tempLib.getBA());
        bulkElementsSize.get(1).setTI(bulkElementsSize.get(1).getTI()+ tempLib.getTI());
    }
    private static void addTwoElemntsBulk2() {
        bulkElementsSize.get(2).setSI( bulkElementsSize.get(2).getSI()+ tempLoc.getSI());
        bulkElementsSize.get(2).setAL(bulkElementsSize.get(2).getAL()+ tempLoc.getAL());
        bulkElementsSize.get(2).setNA(bulkElementsSize.get(2).getNA()+ tempLoc.getNA());
        bulkElementsSize.get(2).setMG(bulkElementsSize.get(2).getMG()+ tempLoc.getMG());
        bulkElementsSize.get(2).setP(bulkElementsSize.get(2).getP()+ tempLoc.getP());
        bulkElementsSize.get(2).setK(bulkElementsSize.get(2).getK()+ tempLoc.getK());
        bulkElementsSize.get(2).setCA(bulkElementsSize.get(2).getCA()+ tempLoc.getCA());
        bulkElementsSize.get(2).setFE(bulkElementsSize.get(2).getFE()+ tempLoc.getFE());
        bulkElementsSize.get(2).setCL(bulkElementsSize.get(2).getCL()+ tempLoc.getCL());
        bulkElementsSize.get(2).setS(bulkElementsSize.get(2).getS()+ tempLoc.getS());
        bulkElementsSize.get(2).setBA(bulkElementsSize.get(2).getBA()+ tempLoc.getBA());
        bulkElementsSize.get(2).setTI(bulkElementsSize.get(2).getTI()+ tempLoc.getTI());

        bulkElementsSize.get(2).setSI( bulkElementsSize.get(2).getSI()+ tempSub.getSI());
        bulkElementsSize.get(2).setAL(bulkElementsSize.get(2).getAL()+ tempSub.getAL());
        bulkElementsSize.get(2).setNA(bulkElementsSize.get(2).getNA()+ tempSub.getNA());
        bulkElementsSize.get(2).setMG(bulkElementsSize.get(2).getMG()+ tempSub.getMG());
        bulkElementsSize.get(2).setP(bulkElementsSize.get(2).getP()+ tempSub.getP());
        bulkElementsSize.get(2).setK(bulkElementsSize.get(2).getK()+ tempSub.getK());
        bulkElementsSize.get(2).setCA(bulkElementsSize.get(2).getCA()+ tempSub.getCA());
        bulkElementsSize.get(2).setFE(bulkElementsSize.get(2).getFE()+ tempSub.getFE());
        bulkElementsSize.get(2).setCL(bulkElementsSize.get(2).getCL()+ tempSub.getCL());
        bulkElementsSize.get(2).setS(bulkElementsSize.get(2).getS()+ tempSub.getS());
        bulkElementsSize.get(2).setBA(bulkElementsSize.get(2).getBA()+ tempSub.getBA());
        bulkElementsSize.get(2).setTI(bulkElementsSize.get(2).getTI()+ tempSub.getTI());

        bulkElementsSize.get(2).setSI( bulkElementsSize.get(2).getSI()+ tempLib.getSI());
        bulkElementsSize.get(2).setAL(bulkElementsSize.get(2).getAL()+ tempLib.getAL());
        bulkElementsSize.get(2).setNA(bulkElementsSize.get(2).getNA()+ tempLib.getNA());
        bulkElementsSize.get(2).setMG(bulkElementsSize.get(2).getMG()+ tempLib.getMG());
        bulkElementsSize.get(2).setP(bulkElementsSize.get(2).getP()+ tempLib.getP());
        bulkElementsSize.get(2).setK(bulkElementsSize.get(2).getK()+ tempLib.getK());
        bulkElementsSize.get(2).setCA(bulkElementsSize.get(2).getCA()+ tempLib.getCA());
        bulkElementsSize.get(2).setFE(bulkElementsSize.get(2).getFE()+ tempLib.getFE());
        bulkElementsSize.get(2).setCL(bulkElementsSize.get(2).getCL()+ tempLib.getCL());
        bulkElementsSize.get(2).setS(bulkElementsSize.get(2).getS()+ tempLib.getS());
        bulkElementsSize.get(2).setBA(bulkElementsSize.get(2).getBA()+ tempLib.getBA());
        bulkElementsSize.get(2).setTI(bulkElementsSize.get(2).getTI()+ tempLib.getTI());
    }


    private static void zeroElementsbulk() {
        for (int i = 0; i <=7 ; i++) {
            bulk = new AllElements();

            bulk.setAL(0);
            bulk.setNA(0);
            bulk.setMG(0);
            bulk.setP(0);
            bulk.setK(0);
            bulk.setCA(0);
            bulk.setFE(0);
            bulk.setCL(0);
            bulk.setS(0);
            bulk.setBA(0);
            bulk.setTI(0);
            bulkElementsSize.add(bulk);
        }
    }

    private static void addTwoElemnts( AllElements tempLoc) {
        bulkOxide.setSI(bulkOxide.getSI()+ tempLoc.getSI());
        bulkOxide.setAL(bulkOxide.getAL()+ tempLoc.getAL());
        bulkOxide.setNA(bulkOxide.getNA()+ tempLoc.getNA());
        bulkOxide.setMG(bulkOxide.getMG()+ tempLoc.getMG());
        bulkOxide.setP(bulkOxide.getP()+ tempLoc.getP());
        bulkOxide.setK(bulkOxide.getK()+ tempLoc.getK());
        bulkOxide.setCA(bulkOxide.getCA()+ tempLoc.getCA());
        bulkOxide.setFE(bulkOxide.getFE()+ tempLoc.getFE());
        bulkOxide.setCL(bulkOxide.getCL()+ tempLoc.getCL());
        bulkOxide.setS(bulkOxide.getS()+ tempLoc.getS());
        bulkOxide.setBA(bulkOxide.getBA()+ tempLoc.getBA());
        bulkOxide.setTI(bulkOxide.getTI()+ tempLoc.getTI());
        totalBulkOxide = bulkOxide.getSI()+ bulkOxide.getAL()+bulkOxide.getNA()+ bulkOxide.getMG()+ bulkOxide.getP()+ bulkOxide.getK()+ bulkOxide.getCA()+ bulkOxide.getFE()+ bulkOxide.getCL()+ bulkOxide.getS()+ bulkOxide.getBA()+ bulkOxide.getTI();
    }

    private static void zeroElements() {
        bulkOxide.setSI(0);
        bulkOxide.setAL(0);
        bulkOxide.setNA(0);
        bulkOxide.setMG(0);
        bulkOxide.setP(0);
        bulkOxide.setK(0);
        bulkOxide.setCA(0);
        bulkOxide.setFE(0);
        bulkOxide.setCL(0);
        bulkOxide.setS(0);
        bulkOxide.setBA(0);
        bulkOxide.setTI(0);



    }

    private static AllElements multElementsLoc(AllElements loc, float percentLoc) {

        tempLoc.setAL(loc.getAL()*percentLoc);
        tempLoc.setNA(loc.getNA()*percentLoc);
        tempLoc.setMG(loc.getMG()*percentLoc);
        tempLoc.setP(loc.getP()*percentLoc);
        tempLoc.setK(loc.getK()*percentLoc);
        tempLoc.setCA(loc.getCA()*percentLoc);
        tempLoc.setFE(loc.getFE()*percentLoc);
        tempLoc.setCL(loc.getCL()*percentLoc);
        tempLoc.setS(loc.getS()*percentLoc);
        tempLoc.setBA(loc.getBA()*percentLoc);
        tempLoc.setTI(loc.getTI()*percentLoc);
        return tempLoc;
    }
    private static AllElements multElementsSub(AllElements loc, float percentLoc) {

        tempSub.setAL(loc.getAL()*percentLoc);
        tempSub.setNA(loc.getNA()*percentLoc);
        tempSub.setMG(loc.getMG()*percentLoc);
        tempSub.setP(loc.getP()*percentLoc);
        tempSub.setK(loc.getK()*percentLoc);
        tempSub.setCA(loc.getCA()*percentLoc);
        tempSub.setFE(loc.getFE()*percentLoc);
        tempSub.setCL(loc.getCL()*percentLoc);
        tempSub.setS(loc.getS()*percentLoc);
        tempSub.setBA(loc.getBA()*percentLoc);
        tempSub.setTI(loc.getTI()*percentLoc);
        return tempSub;
    }
    private static AllElements multElementsLib(AllElements loc, float percentLoc) {

        tempLib.setAL(loc.getAL()*percentLoc);
        tempLib.setNA(loc.getNA()*percentLoc);
        tempLib.setMG(loc.getMG()*percentLoc);
        tempLib.setP(loc.getP()*percentLoc);
        tempLib.setK(loc.getK()*percentLoc);
        tempLib.setCA(loc.getCA()*percentLoc);
        tempLib.setFE(loc.getFE()*percentLoc);
        tempLib.setCL(loc.getCL()*percentLoc);
        tempLib.setS(loc.getS()*percentLoc);
        tempLib.setBA(loc.getBA()*percentLoc);
        tempLib.setTI(loc.getTI()*percentLoc);
        return tempLib;
    }
    private static AllElements divlibOxideElements(AllElements oxide, float phaseSum2Div100) {
        lib.setSI(oxide.getSI()/phaseSum2Div100);
        lib.setAL(oxide.getAL()/phaseSum2Div100);
        lib.setNA(oxide.getNA()/phaseSum2Div100);
        lib.setMG(oxide.getMG()/phaseSum2Div100);
        lib.setP(oxide.getP()/phaseSum2Div100);
        lib.setK(oxide.getK()/phaseSum2Div100);
        lib.setCA(oxide.getCA()/phaseSum2Div100);
        lib.setFE(oxide.getFE()/phaseSum2Div100);
        lib.setCL(oxide.getCL()/phaseSum2Div100);
        lib.setS(oxide.getS()/phaseSum2Div100);
        lib.setBA(oxide.getBA()/phaseSum2Div100);
        lib.setTI(oxide.getTI()/phaseSum2Div100);
        return lib;
    }

    private static AllElements divlocOxideElements(AllElements oxide, float phaseSum2Div100) {
        loc.setSI(oxide.getSI()/phaseSum2Div100);
        loc.setAL(oxide.getAL()/phaseSum2Div100);
        loc.setNA(oxide.getNA()/phaseSum2Div100);
        loc.setMG(oxide.getMG()/phaseSum2Div100);
        loc.setP(oxide.getP()/phaseSum2Div100);
        loc.setK(oxide.getK()/phaseSum2Div100);
        loc.setCA(oxide.getCA()/phaseSum2Div100);
        loc.setFE(oxide.getFE()/phaseSum2Div100);
        loc.setCL(oxide.getCL()/phaseSum2Div100);
        loc.setS(oxide.getS()/phaseSum2Div100);
        loc.setBA(oxide.getBA()/phaseSum2Div100);
        loc.setTI(oxide.getTI()/phaseSum2Div100);

        return loc;
    }


    private static void FindBulkCompositions(AllElements bulk, Float[] cut2) {
        float tmp1, tmp2, tmp3, tmp4;

        tmp1 = cut2[1]/100;
        tmp2 = cut2[2]/ 100;
        tmp3 = cut2[3]/100;
        tmp4 = cut2[4]/100;
        bulkElementsSize.get(7).setNA(tmp1 * bulkElementsSize.get(1).getNA()+ tmp2 * bulkElementsSize.get(2).getNA()+ tmp3 * bulkElementsSize.get(3).getNA()+tmp4 * bulkElementsSize.get(4).getNA());
        bulkElementsSize.get(7).setMG(tmp1 * bulkElementsSize.get(1).getMG()+tmp2 * bulkElementsSize.get(2).getMG() + tmp3 * bulkElementsSize.get(3).getMG()+ tmp4 * bulkElementsSize.get(4).getMG());
        bulkElementsSize.get(7).setAL(tmp1 * bulkElementsSize.get(1).getAL()+tmp2 * bulkElementsSize.get(2).getAL()+ tmp3 * bulkElementsSize.get(3).getAL()+ tmp4 * bulkElementsSize.get(4).getAL() );
        bulkElementsSize.get(7).setSI(tmp1 * bulkElementsSize.get(1).getSI()+ tmp2 * bulkElementsSize.get(2).getSI()+ tmp3 * bulkElementsSize.get(3).getSI()+tmp4 * bulkElementsSize.get(4).getSI());
        bulkElementsSize.get(7).setP(tmp1 * bulkElementsSize.get(1).getP()+ tmp2 * bulkElementsSize.get(2).getP()+ tmp3 * bulkElementsSize.get(3).getP()+ tmp4 * bulkElementsSize.get(4).getP());
        bulkElementsSize.get(7).setS(tmp1 * bulkElementsSize.get(1).getS()+ tmp2 * bulkElementsSize.get(2).getS()+ tmp3 * bulkElementsSize.get(3).getS()+ tmp4 * bulkElementsSize.get(4).getS());
        bulkElementsSize.get(7).setK(tmp1 * bulkElementsSize.get(1).getK()+ tmp2 * bulkElementsSize.get(2).getK()+ tmp3 * bulkElementsSize.get(3).getK()+ tmp4 * bulkElementsSize.get(4).getK());
        bulkElementsSize.get(7).setCL(tmp1 * bulkElementsSize.get(1).getCL()+ tmp2 * bulkElementsSize.get(2).getCL()+ tmp3 * bulkElementsSize.get(3).getCL()+ tmp4 * bulkElementsSize.get(4).getCL());
        bulkElementsSize.get(7).setCA(tmp1 * bulkElementsSize.get(1).getCA() + tmp2 * bulkElementsSize.get(2).getCA()+ tmp3 * bulkElementsSize.get(3).getCA()+ tmp4 * bulkElementsSize.get(4).getCA());
        bulkElementsSize.get(7).setBA(tmp1 * bulkElementsSize.get(1).getBA()+ tmp2 * bulkElementsSize.get(2).getBA()+ tmp3 * bulkElementsSize.get(3).getBA()+ tmp4 * bulkElementsSize.get(4).getBA());
        bulkElementsSize.get(7).setTI(tmp1 * bulkElementsSize.get(1).getTI()+ tmp2 * bulkElementsSize.get(2).getTI()+ tmp3 * bulkElementsSize.get(3).getTI()+ tmp4 * bulkElementsSize.get(4).getTI());
        bulkElementsSize.get(7).setFE(tmp1 * bulkElementsSize.get(1).getFE()+ tmp2 * bulkElementsSize.get(2).getFE()+ tmp3 * bulkElementsSize.get(3).getFE()+ tmp4 * bulkElementsSize.get(4).getFE());
    }

    private static double findElementTotal(AllElements allElements) {
        float t;
        t = allElements.getSI()+ allElements.getAL()+ allElements.getNA()+ allElements.getMG()+ allElements.getP()+ allElements.getK()+ allElements.getCA()+ allElements.getFE()+ allElements.getCL()+ allElements.getS()+ allElements.getBA()+ allElements.getTI();
        return t;
    }

}
