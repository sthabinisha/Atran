package com.ccsem;

import com.ccsem.common.CCSEMStorageUnit;
import com.ccsem.common.CompoundName;
import com.ccsem.common.FileIO;
import com.ccsem.common.GetPhase;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static com.ccsem.common.Data.*;

public class App extends JFrame{
    private JButton getMagFile;
    private JPanel panelMain;
    private JPanel panelTop;
    private JPanel panelMid;
    private JTextField username;
    private JButton submitButton;
    private JButton browseButton;
    private JTextField fundNo;
    private JTextField mag50Text;
    private JTextField mag250text;
    private JTextField nameprnText;
    private JButton mag50;
    private JButton mag250;
    private JButton nameprn;
    private JPanel SecondMain;
    private JPanel Front;
    private JPanel second;
    private JPanel panel3;
    private JTextField boilerTypeText;
    private JButton processAtranButton;
    private String[] nameselement = new String[34];
    public static Float[] sizeRange = new Float[8];
    public static Float[] cut2 = new Float[5];
    public static String[] sizeName= new String[7];
    private static float  partMax;
    private static float[] size = new float[7];
    private static float[][][] myarray ;
    public static ArrayList<CCSEMStorageUnit> mag1 = new ArrayList<>();
    public static ArrayList<CCSEMStorageUnit> mag2 = new ArrayList<>();

    public static CCSEMStorageUnit storageUnit;
    public static CCSEMStorageUnit storageUnits;


    public static float maxSize, correction[] = new float[3];

    public static int locLib, locLibT, frame, point = 0;
    public static float frager, frag1, frag2, frag3, frag4, frag5, frag6, frag7, frag9, frag10, newArea, newDiameter, normSum, freq, temp, na, mg, al, si, p, s, cl, k, ca, fe, ba, ti, area, avgDiameter;
    public static String boilerType;

    public static ArrayList<String> allSulT = new ArrayList<String>(), magLocT = new ArrayList<String>(), magLibT = new ArrayList<String>(), locTypT = new ArrayList<String>(), libTypT = new ArrayList<String>();

    public static String ultimate;
    public static String xray;
    public static String[] magAll;
    ;
    public static ArrayList<String> magnification = new ArrayList<>();
    public static String[][][] allSul = new String[5][3][15];
    public static String[][][] data = new String[5][3][15];
    public static String[][][] magLoc = new String[5][3][15];
    public static String[][][] magLib = new String[5][3][15];
    public static String[][][] locTyp = new String[5][3][15];
    public static String[][][] libtyp = new String[5][3][15];
    public static ArrayList<CCSEMStorageUnit> mag2storageUnits = new ArrayList<>();

    public static ArrayList<CCSEMStorageUnit> storagePhase = new ArrayList<>();

    public static int[] hiPartSize = new int[3];
    public static int[] loPartSize = new int[3];



    public static Random randum;


    public static float diaRatio;
//    public static FileIO fileio;
    public static float[] totalWeight = new float[6], sitWeight = new float[6];
    //			FloatContainer hiPartSize = new FloatContainer(3), loPartSize = new FloatContainer(3);
    public static float peRcSiLoc = 0, percLoc = 0;
    public static float coalEsce = 0;
    public static int[] excludedCount = new int [34];
    public static float[][] totar ;

    //public static double coalArea;


    public static int[] maxFrame = {0, 0, 0, 0, 0, 0};

    public static int i = 0, blend, numMag = 2;
    public static File comboAshFile;
    public static CompoundName compoundName;
    public static GetPhase getPhase;
    public static FileIO fileio ;


//    public  static String[] nameselement = new String[34];
//    public static float  partMax;
//    public static float[] size = new float[7];




    JFileChooser fileChooser = new JFileChooser();


    public App(float[][][] myarrays, float[][] totars) {
        add(panelMain);
        setTitle("ATRAN");
        setSize(500, 500);
        setLocationRelativeTo(null);
         fileio = new FileIO();
        getPhase = new GetPhase();
        randum = new Random();
        compoundName = new CompoundName();
        storageUnits = new CCSEMStorageUnit();

        this.myarray = myarrays;
        this.totar = totars;



        mag250.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                float outVolume;
                if(fileChooser.showOpenDialog(null)==JFileChooser.APPROVE_OPTION){
                    File file = fileChooser.getSelectedFile();
                    mag250text.setText(fileChooser.getSelectedFile().getAbsolutePath());

                    mag1 = new ArrayList<>();
                    mag1 = readmagFile(file, mag1);

                    storageUnits.setMagnification1(mag1);


                }

            }
        });
        mag50.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                float outVolume;

                if(fileChooser.showOpenDialog(null)==JFileChooser.APPROVE_OPTION){
                    mag50Text.setText(fileChooser.getSelectedFile().getAbsolutePath());
                    File file = fileChooser.getSelectedFile();
                    mag2 = new ArrayList<>();

                    mag2 = readmagFile(file, mag2);
                    storageUnits.setMagnification2(mag2);


                    }

            }


        });
        nameprn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(fileChooser.showOpenDialog(null)==JFileChooser.APPROVE_OPTION){
                    File file = fileChooser.getSelectedFile();
                    nameprnText.setText(fileChooser.getSelectedFile().getAbsolutePath());
                    readNameFile(file);
                    //readFile(file);

                }

            }
        });


        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();

        Date date = new Date();
        Calendar cal = Calendar.getInstance();


        setLocationRelativeTo(null);
        setVisible(true);
        submitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                submitButton.setText("loading...");
                new SwingWorker<Void, String>() {
                    @Override
                    protected Void doInBackground() throws Exception {
                        // Worken hard or hardly worken...
                        Thread.sleep(10000);

                        String user = username.getText();
                        String fundNoText = fundNo.getText();
                        boilerType = boilerTypeText.getText();
                        //temp
//                        boilerType = "Pulverized";
//                        mag1 = new ArrayList<>();
//
//                        mag1 = readmagFile(new File("magnification1.xls"), mag1);
//
//                        storageUnits.setMagnification1(mag1);
//                        mag2 = new ArrayList<>();
//                        mag2 = readmagFile(new File("magnification.xls"), mag2);
//
//                        storageUnits.setMagnification2(mag2);
//
//                        readNameFile(new File("Name.xls"));
                        //temp


//
                        fileio.tprintf(sumFile, "Summary of ASH PREDICTION results: PROG VERSION 2BF " );
                        fileio.tprintf(sumFile, "SAMPLE DESCRIPTION ----> " + user);
                        fileio.tprintf(sumFile, "SUBMITTER          ----> " + user);
                        fileio.tprintf(sumFile, "ICC # AND FUND #   ----> " + fundNoText);
                        fileio.tprintf(sumFile, "RUN DATE AND TIME  ----> " + dateFormat.format(now));
                        fileio.tprintf(sumFile, "SUMMARY OF PARAMETERS " );
                        // System.out.println("Done writing to " + fileName); //For testing

                        if(mag1.size()==0 || mag2.size()==0 ){
                            Thread.sleep(5000);
                        }
                        for (int i = 0; i < mag1.size(); i++) {
                            mag1.get(i).setPhase(getPhase.getPhase(mag1.get(i).getAl(), mag1.get(i).getBa(), mag1.get(i).getCa(), mag1.get(i).getCl(), mag1.get(i).getFe(), mag1.get(i).getK(), mag1.get(i).getMg(), mag1.get(i).getNA(), mag1.get(i).getP(), mag1.get(i).getS(), mag1.get(i).getSi(), mag1.get(i).getTi()));

                            avgDiameter = (float) (mag1.get(i).getAvgDiameter() * Math.pow(myarray[9][1][(int) mag1.get(i).getPhase()], (1 / 2)));
                            float outVolume = fileio.volumeSphere(avgDiameter / 2);
                            findSize(avgDiameter, mag1.get(i).getArea(), outVolume, (int) mag1.get(i).getPhase(), (int) mag1.get(i).getLocLib());

                        }
                        for (int i = 0; i < mag2.size(); i++) {
                            mag2.get(i).setPhase(getPhase.getPhase(mag2.get(i).getAl(), mag2.get(i).getBa(), mag2.get(i).getCa(), mag2.get(i).getCl(), mag2.get(i).getFe(), mag2.get(i).getK(), mag2.get(i).getMg(), mag2.get(i).getNA(), mag2.get(i).getP(), mag2.get(i).getS(), mag2.get(i).getSi(), mag2.get(i).getTi()));

                            avgDiameter = (float) (mag2.get(i).getAvgDiameter() * Math.pow(myarray[9][1][(int) mag2.get(i).getPhase()], (1 / 2)));
                            float outVolume = fileio.volumeSphere(avgDiameter / 2);
                            findSize(avgDiameter, mag2.get(i).getArea(), outVolume, (int) mag2.get(i).getPhase(), (int) mag2.get(i).getLocLib());

                        }

                        matFil(coalArea, 1);
                        writeIntoFirstFile( user, fundNoText, boilerType, dateFormat.format((now)));




                        if (boilerType.equals("Fluidized")) {
                            frag1 = 20; //PYRITE
                            frag2 = 20;// IRON CARBONATE
                            frag3 = 20;// PYRRHOTITE & OX. PYRRHOTITE
                            frag4 = 1;//GYPSUM
                            frag5 = 1; // BARITE
                            frag6 = 1; //CA-AL-P
                            frag7 = 30; //KAOLINITE
                            frag9 = 9; // CALCITE & DOLOMITE
                            frag10 = 15; // QUATZ
                            coalEsce = 70;
                        } else if (boilerType.equals("Pulverized")) {
                            frag1 = 5; //PYRITE
                            frag2 = 5;// IRON CARBONATE
                            frag3 = 5;// PYRRHOTITE & OX. PYRRHOTITE
                            frag4 = 1;//GYPSUM
                            frag5 = 1;// BARITE
                            frag6 = 1;//CA-AL-P
                            frag7 = 70; //KAOLINITE
                            frag9 = 1; // CALCITE & DOLOMITE
                            frag10 = 150;// QUATZ
                            coalEsce = 150;
                        } else if (boilerType.equals("Low")) {
                            frag1 = 7;//PYRITE
                            frag2 = 7;// IRON CARBONATE
                            frag3 = 7;// PYRRHOTITE & OX. PYRRHOTITE
                            frag4 = 1;//GYPSUM
                            frag5 = 1;// BARITE
                            frag6 = 1;//CA-AL-P
                            frag7 = 20; //KAOLINITE
                            frag9 = 1;// CALCITE & DOLOMITE
                            frag10 = 25;// QUATZ
                            coalEsce = 240;
                        } else if (boilerType.equals("Cyclone")) {
                            frag1 = 350;//PYRITE
                            frag2 = 350;// IRON CARBONATE
                            frag3 = 390;// PYRRHOTITE & OX. PYRRHOTITE
                            frag4 = 1;//GYPSUM
                            frag5 = 1;// BARITE
                            frag6 = 1;//CA-AL-P
                            frag7 = 100; //KAOLINITE
                            frag9 = 1;// CALCITE & DOLOMITE
                            frag10 = 1;// QUATZ
                            coalEsce = 420;
                        } else {
                            System.out.println("No Boiler type found ");

                        }
                        return null;
                    }

                    @Override
                    protected void done() {
                        Front.setVisible(false);
                        second.setVisible(true);
                        processAtranButton.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                ProcessAtran processAtran = new ProcessAtran();
                                processAtran.AtranProcess( storagePhase, coalEsce, username.getText(), fundNo.getText(), boilerType,storageUnits, nameselement, partMax, myarray, size, sizeRange, sizeName);

                            }
                        });
                    }
                }.execute();



                }


        });
    }

    private void writeIntoFirstFile( String user, String fundNoText, String boilerType, String date) {

        fileio.tprintf(firstFile, "Summary of ASH PREDICTION results: PROG VERSION 1 " );
        fileio.tprintf(firstFile, "SAMPLE DESCRIPTION ----> " + user);
        fileio.tprintf(firstFile, "SUBMITTER          ----> " + user);
        fileio.tprintf(firstFile, "ICC # AND FUND #   ----> " + fundNoText);
        fileio.tprintf(firstFile, "RUN DATE AND TIME  ----> " + date);
        fileio.tprintf(firstFile, "SUMMARY OF PARAMETERS " );
        //matFil( coalArea, 1);

        storagePhase.addAll(mag1);
        storagePhase.addAll(mag2);

        //matFil(coalArea, 1);


        HashMap<Integer, List<Integer>> hashmap = new HashMap<Integer, List<Integer>>();
            HashMap<Integer, List<Integer>> hashmapM = new HashMap<Integer, List<Integer>>();

            List<Integer> curVal = hashmap.get(storagePhase.get(i).getPhase());
            HashMap<Integer, List<Integer>> hashmap1 = new HashMap<Integer, List<Integer>>();
            HashMap<Integer, List<Integer>> hashmap1M = new HashMap<Integer, List<Integer>>();

            HashMap<Integer, List<Integer>> hashmap0 = new HashMap<Integer, List<Integer>>();
            HashMap<Integer, List<Integer>> hashmap0M = new HashMap<Integer, List<Integer>>();


            List<Integer> curVal1 = hashmap.get(storagePhase.get(i).getPhase());

            List<Integer> curVal0 = hashmap.get(storagePhase.get(i).getPhase());
            fileio.tprintf(firstFile, "\n\n\n");
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
            LocalDateTime now = LocalDateTime.now();
            fileio.tprintf(firstFile, "Summary of parameters " + dtf.format(now));
            fileio.tprintf(firstFile, String.format("%1$" + 18 + "s", " ") + "  " + String.format("%1$" + 7 + "s", "NA") + "  " + String.format("%1$" + 7 + "s", "MG") + "  " + String.format("%1$" + 7 + "s", "AL") + "  " + String.format("%1$" + 7 + "s", "SI") + "  " + String.format("%1$" + 7 + "s", "P") + "  " + String.format("%1$" + 7 + "s", "S") + "  " + String.format("%1$" + 7 + "s", "CL") + "  " + String.format("%1$" + 7 + "s", "K2") + "  " + String.format("%1$" + 7 + "s", "CAO") + "  " + String.format("%1$" + 7 + "s", "FE") + "  " + String.format("%1$" + 7 + "s", "BA") + "  " + String.format("%1$" + 7 + "s", "TI"));


            for (int i = 0; i < storagePhase.size(); i++) {

                storagePhase.get(i).setPhase(getPhase.getPhase(storagePhase.get(i).getAl(), storagePhase.get(i).getBa(), storagePhase.get(i).getCa(), storagePhase.get(i).getCl(), storagePhase.get(i).getFe(), storagePhase.get(i).getK(), storagePhase.get(i).getMg(), storagePhase.get(i).getNA(), storagePhase.get(i).getP(), storagePhase.get(i).getS(), storagePhase.get(i).getSi(), storagePhase.get(i).getTi()));

                if (hashmap.containsKey((int) storagePhase.get(i).getPhase())) {
                    hashmap.get((int) storagePhase.get(i).getPhase()).add(i);
                } else {
                    curVal = new ArrayList<Integer>();
                    curVal.add(i);
                    hashmap.put((int) storagePhase.get(i).getPhase(), curVal);
                }
                if (storagePhase.get(i).getLocLib() == 1) {
                    if (hashmap1.containsKey((int) storagePhase.get(i).getPhase())) {
                        hashmap1.get((int) storagePhase.get(i).getPhase()).add(i);
                    } else {
                        curVal1 = new ArrayList<Integer>();
                        curVal1.add(i);
                        hashmap1.put((int) storagePhase.get(i).getPhase(), curVal1);
                    }
                } else {
                    if (hashmap0.containsKey((int) storagePhase.get(i).getPhase())) {
                        hashmap0.get((int) storagePhase.get(i).getPhase()).add(i);
                    } else {
                        curVal0 = new ArrayList<Integer>();
                        curVal0.add(i);
                        hashmap0.put((int) storagePhase.get(i).getPhase(), curVal0);
                    }
                }


            }


            for (Map.Entry<Integer, List<Integer>> entry : hashmap.entrySet()) {
//            for(int i = 1; i<= 33; i++){
//                if(hashmap.containsKey(i)) {
                float sumNa = 0, sumMg = 0, sumAl = 0, sumsi = 0, sumP = 0, sumS = 0, sumCl = 0, sumCa = 0, sumK = 0, sumFe = 0, sumBa = 0, sumTi = 0;

                for (int j = 0; j < entry.getValue().size(); j++) {

                    sumNa += storagePhase.get(entry.getValue().get(j)).getNA();
                    sumMg += storagePhase.get(entry.getValue().get(j)).getMg();
                    sumAl += storagePhase.get(entry.getValue().get(j)).getAl();
                    sumsi += storagePhase.get(entry.getValue().get(j)).getSi();
                    sumS += storagePhase.get(entry.getValue().get(j)).getS();
                    sumBa += storagePhase.get(entry.getValue().get(j)).getBa();
                    sumP += storagePhase.get(entry.getValue().get(j)).getP();
                    sumK += storagePhase.get(entry.getValue().get(j)).getK();
                    sumTi += storagePhase.get(entry.getValue().get(j)).getTi();
                    sumFe += storagePhase.get(entry.getValue().get(j)).getFe();
                    sumCa += storagePhase.get(entry.getValue().get(j)).getCa();
                    sumCl += storagePhase.get(entry.getValue().get(j)).getCl();
                }
                sumNa /= entry.getValue().size();
                sumMg /= entry.getValue().size();
                sumAl /= entry.getValue().size();
                sumsi /= entry.getValue().size();
                sumS /= entry.getValue().size();
                sumBa /= entry.getValue().size();
                sumP /= entry.getValue().size();
                sumK /= entry.getValue().size();
                sumTi /= entry.getValue().size();
                sumFe /= entry.getValue().size();
                sumCa /= entry.getValue().size();
                sumCl /= entry.getValue().size();
                float norm = sumNa + sumMg + sumAl + sumsi + sumS + sumBa + sumP + sumK + sumTi + sumFe + sumCa + sumCl;
                float temps;
                if (norm != 0)
                    temps = 100 / norm;
                else
                    temps = 100;

                sumNa *= temps;

                sumMg *= temps;
                sumAl *= temps;
                sumsi *= temps;
                sumS *= temps;
                sumBa *= temps;
                sumP *= temps;
                sumK *= temps;
                sumTi *= temps;
                sumFe *= temps;
                sumCa *= temps;
                sumCl *= temps;
                fileio.tprintf(firstFile, String.format("%1$" + 20 + "s", compoundName.Name(entry.getKey())) + " " + String.format("%1$" + 7 + "s", df.format(sumNa)) + "  " + String.format("%1$" + 7 + "s", df.format(sumMg)) + "  " + String.format("%1$" + 7 + "s", df.format(sumAl)) + "  " + String.format("%1$" + 7 + "s", df.format(sumsi)) + "  " + String.format("%1$" + 7 + "s", df.format(sumP)) + "  " + String.format("%1$" + 7 + "s", df.format(sumS)) + "  " + String.format("%1$" + 7 + "s", df.format(sumCl)) + "  " + String.format("%1$" + 7 + "s", df.format(sumK)) + "  " + String.format("%1$" + 7 + "s", df.format(sumCa)) + "  " + String.format("%1$" + 7 + "s", df.format(sumFe)) + "  " + String.format("%1$" + 7 + "s", df.format(sumBa)) + "  " + String.format("%1$" + 7 + "s", df.format(sumTi)));

            }


            fileio.tprintf(firstFile, "\n\nSummary of liberated ");
            fileio.tprintf(firstFile, String.format("%1$" + 18 + "s", " ") + "  " + String.format("%1$" + 7 + "s", "NA") + "  " + String.format("%1$" + 7 + "s", "MG") + "  " + String.format("%1$" + 7 + "s", "AL") + "  " + String.format("%1$" + 7 + "s", "SI") + "  " + String.format("%1$" + 7 + "s", "P") + "  " + String.format("%1$" + 7 + "s", "S") + "  " + String.format("%1$" + 7 + "s", "CL") + "  " + String.format("%1$" + 7 + "s", "K2") + "  " + String.format("%1$" + 7 + "s", "CAO") + "  " + String.format("%1$" + 7 + "s", "FE") + "  " + String.format("%1$" + 7 + "s", "BA") + "  " + String.format("%1$" + 7 + "s", "TI")/*+ "  " + String.format("%1$" + 10 + "s", "Excluded")*/);


            for (Map.Entry<Integer, List<Integer>> entry : hashmap1.entrySet()) {
//            for(int i = 1; i<= 33; i++){
//                if(hashmap.containsKey(i)) {
                float sumNa = 0, sumMg = 0, sumAl = 0, sumsi = 0, sumP = 0, sumS = 0, sumCl = 0, sumCa = 0, sumK = 0, sumFe = 0, sumBa = 0, sumTi = 0;

                for (int j = 0; j < entry.getValue().size(); j++) {

                    sumNa += storagePhase.get(entry.getValue().get(j)).getNA();
                    sumMg += storagePhase.get(entry.getValue().get(j)).getMg();
                    sumAl += storagePhase.get(entry.getValue().get(j)).getAl();
                    sumsi += storagePhase.get(entry.getValue().get(j)).getSi();
                    sumS += storagePhase.get(entry.getValue().get(j)).getS();
                    sumBa += storagePhase.get(entry.getValue().get(j)).getBa();
                    sumP += storagePhase.get(entry.getValue().get(j)).getP();
                    sumK += storagePhase.get(entry.getValue().get(j)).getK();
                    sumTi += storagePhase.get(entry.getValue().get(j)).getTi();
                    sumFe += storagePhase.get(entry.getValue().get(j)).getFe();
                    sumCa += storagePhase.get(entry.getValue().get(j)).getCa();
                    sumCl += storagePhase.get(entry.getValue().get(j)).getCl();
                }
                sumNa /= entry.getValue().size();
                sumMg /= entry.getValue().size();
                sumAl /= entry.getValue().size();
                sumsi /= entry.getValue().size();
                sumS /= entry.getValue().size();
                sumBa /= entry.getValue().size();
                sumP /= entry.getValue().size();
                sumK /= entry.getValue().size();
                sumTi /= entry.getValue().size();
                sumFe /= entry.getValue().size();
                sumCa /= entry.getValue().size();
                sumCl /= entry.getValue().size();
                float norm = sumNa + sumMg + sumAl + sumsi + sumS + sumBa + sumP + sumK + sumTi + sumFe + sumCa + sumCl;
                float temps;
                if (norm != 0)
                    temps = 100 / norm;
                else
                    temps = 100;

                sumNa *= temps;

                sumMg *= temps;
                sumAl *= temps;
                sumsi *= temps;
                sumS *= temps;
                sumBa *= temps;
                sumP *= temps;
                sumK *= temps;
                sumTi *= temps;
                sumFe *= temps;
                sumCa *= temps;
                sumCl *= temps;
                fileio.tprintf(firstFile, String.format("%1$" + 20 + "s", compoundName.Name(entry.getKey())) + " " + String.format("%1$" + 7 + "s", df.format(sumNa)) + "  " + String.format("%1$" + 7 + "s", df.format(sumMg)) + "  " + String.format("%1$" + 7 + "s", df.format(sumAl)) + "  " + String.format("%1$" + 7 + "s", df.format(sumsi)) + "  " + String.format("%1$" + 7 + "s", df.format(sumP)) + "  " + String.format("%1$" + 7 + "s", df.format(sumS)) + "  " + String.format("%1$" + 7 + "s", df.format(sumCl)) + "  " + String.format("%1$" + 7 + "s", df.format(sumK)) + "  " + String.format("%1$" + 7 + "s", df.format(sumCa)) + "  " + String.format("%1$" + 7 + "s", df.format(sumFe)) + "  " + String.format("%1$" + 7 + "s", df.format(sumBa)) + "  " + String.format("%1$" + 7 + "s", df.format(sumTi))/*+ "  " +String.format("%1$" + 10 + "s", df.format(entry.getValue().size()* 100/33))*/);


            }


            fileio.tprintf(firstFile, "\n\nSummary of locked ");
            fileio.tprintf(firstFile, String.format("%1$" + 18 + "s", " ") + "  " + String.format("%1$" + 7 + "s", "NA") + "  " + String.format("%1$" + 7 + "s", "MG") + "  " + String.format("%1$" + 7 + "s", "AL") + "  " + String.format("%1$" + 7 + "s", "SI") + "  " + String.format("%1$" + 7 + "s", "P") + "  " + String.format("%1$" + 7 + "s", "S") + "  " + String.format("%1$" + 7 + "s", "CL") + "  " + String.format("%1$" + 7 + "s", "K2") + "  " + String.format("%1$" + 7 + "s", "CAO") + "  " + String.format("%1$" + 7 + "s", "FE") + "  " + String.format("%1$" + 7 + "s", "BA") + "  " + String.format("%1$" + 7 + "s", "TI")/*+ "  " + String.format("%1$" + 10 + "s", "Excluded")*/);

            for (Map.Entry<Integer, List<Integer>> entry : hashmap0.entrySet()) {
//            for(int i = 1; i<= 33; i++){
//                if(hashmap.containsKey(i)) {
                float sumNa = 0, sumMg = 0, sumAl = 0, sumsi = 0, sumP = 0, sumS = 0, sumCl = 0, sumCa = 0, sumK = 0, sumFe = 0, sumBa = 0, sumTi = 0;

                for (int j = 0; j < entry.getValue().size(); j++) {

                    sumNa += storagePhase.get(entry.getValue().get(j)).getNA();
                    sumMg += storagePhase.get(entry.getValue().get(j)).getMg();
                    sumAl += storagePhase.get(entry.getValue().get(j)).getAl();
                    sumsi += storagePhase.get(entry.getValue().get(j)).getSi();
                    sumS += storagePhase.get(entry.getValue().get(j)).getS();
                    sumBa += storagePhase.get(entry.getValue().get(j)).getBa();
                    sumP += storagePhase.get(entry.getValue().get(j)).getP();
                    sumK += storagePhase.get(entry.getValue().get(j)).getK();
                    sumTi += storagePhase.get(entry.getValue().get(j)).getTi();
                    sumFe += storagePhase.get(entry.getValue().get(j)).getFe();
                    sumCa += storagePhase.get(entry.getValue().get(j)).getCa();
                    sumCl += storagePhase.get(entry.getValue().get(j)).getCl();
                }
                sumNa /= entry.getValue().size();
                sumMg /= entry.getValue().size();
                sumAl /= entry.getValue().size();
                sumsi /= entry.getValue().size();
                sumS /= entry.getValue().size();
                sumBa /= entry.getValue().size();
                sumP /= entry.getValue().size();
                sumK /= entry.getValue().size();
                sumTi /= entry.getValue().size();
                sumFe /= entry.getValue().size();
                sumCa /= entry.getValue().size();
                sumCl /= entry.getValue().size();
                float norm = sumNa + sumMg + sumAl + sumsi + sumS + sumBa + sumP + sumK + sumTi + sumFe + sumCa + sumCl;
                float temps;
                if (norm != 0)
                    temps = 100 / norm;
                else
                    temps = 100;

                sumNa *= temps;

                sumMg *= temps;
                sumAl *= temps;
                sumsi *= temps;
                sumS *= temps;
                sumBa *= temps;
                sumP *= temps;
                sumK *= temps;
                sumTi *= temps;
                sumFe *= temps;
                sumCa *= temps;
                sumCl *= temps;
                fileio.tprintf(firstFile, String.format("%1$" + 20 + "s", compoundName.Name(entry.getKey())) + " " + String.format("%1$" + 7 + "s", df.format(sumNa)) + "  " + String.format("%1$" + 7 + "s", df.format(sumMg)) + "  " + String.format("%1$" + 7 + "s", df.format(sumAl)) + "  " + String.format("%1$" + 7 + "s", df.format(sumsi)) + "  " + String.format("%1$" + 7 + "s", df.format(sumP)) + "  " + String.format("%1$" + 7 + "s", df.format(sumS)) + "  " + String.format("%1$" + 7 + "s", df.format(sumCl)) + "  " + String.format("%1$" + 7 + "s", df.format(sumK)) + "  " + String.format("%1$" + 7 + "s", df.format(sumCa)) + "  " + String.format("%1$" + 7 + "s", df.format(sumFe)) + "  " + String.format("%1$" + 7 + "s", df.format(sumBa)) + "  " + String.format("%1$" + 7 + "s", df.format(sumTi))/*+ "  " +String.format("%1$" + 10 + "s", df.format(entry.getValue().size()* 100/33))*/);


            }


            //here

            fileio.tprintf(firstFile, "\n\nSUMMARY OF WEIGHT PERCENT ON MINERAL BASIS  " );
            fileio.tprintf(firstFile,"RUN DATE AND TIME " + dtf.format(now));
            fileio.tprintf(firstFile, "PERCENT EPOXY USED      " + "50");
            // fileio.tprintf(comboashfileName, "TOTAL MINERAL AREA ANALYZED    ----> " + totar[1][7]);
            fileio.tprintf(firstFile, "TOTAL MINERAL AREA ANALYZED    ----> " + totar[1][7]);
            fileio.tprintf(firstFile, "FIELD SIZE USED                 ----> " +  Math.pow((((((Math.sqrt(115538.8) + 10.0) / 4.167) * 1000) / 240) - (2 * 5)), 2));
            fileio.tprintf(firstFile, "NUMBER OF FRAMES                ----> " + 2+ "\n\n");


            fileio.tprintf(firstFile, String.format("%1$" + 27 + "s", "1.0") + "    2.2     4.6    10    22      46  ");
            fileio.tprintf(firstFile, String.format("%1$" + 27 + "s", "TO") + "      TO     TO     TO    TO     TO    TOTALS   EXCLUDED");
            fileio.tprintf(firstFile, String.format("%1$" + 27 + "s", "2.2") + "     4.6     10     22    46     100");
            fileio.tprintf(firstFile, "-----------------------------------------------------------------------------------------\n");
            for (int i = 1; i <= 33; i++) {
                fileio.tprintfn(firstFile, String.format("%1$" + 20 + "s", nameselement[i]));
                fileio.tprintf(firstFile, "  " + String.format("%1$" + 5 + "s", df.format(myarray[4][1][i])) + "  " + String.format("%1$" + 5 + "s", df.format(myarray[4][2][i])) + "  " + String.format("%1$" + 5 + "s", df.format(myarray[4][3][i])) + "  " + String.format("%1$" + 5 + "s", df.format(myarray[4][4][i])) + "  " + String.format("%1$" + 5 + "s", df.format(myarray[4][5][i])) + "  " + String.format("%1$" + 5 + "s", df.format(myarray[4][6][i])) + "  " + String.format("%1$" + 5 + "s", df.format(myarray[4][7][i]))+ "  " + String.format("%1$" + 5 + "s", df.format(excludedCount[i]/myarray[4][7][i])));
                //fileio.tprintf(comboashfileName, "  " + String.format("%1$" + 5 + "s", String.format("%.8f",myarray[4][1][i])) + "  " + String.format("%1$" + 5 + "s", myarray[4][2][i]) + "  " + String.format("%1$" + 5 + "s", myarray[4][3][i]) + "  " + String.format("%1$" + 5 + "s", myarray[4][4][i]) + "  " + String.format("%1$" + 5 + "s", myarray[4][5][i]) + "  " + String.format("%1$" + 5 + "s", myarray[4][6][i]) + "  " + String.format("%1$" + 5 + "s", myarray[4][7][i]));


            }
            fileio.tprintf(firstFile, "-----------------------------------------------------------------------------------------\n");
            fileio.tprintf(firstFile, String.format("%1$"+20+ "s", "TOTALS")+ "   "  + String.format("%1$"+5+ "s",df.format(totar[4][1] ))+"  "+String.format("%1$"+5+ "s",df.format(totar[4][2] )) +"  "+String.format("%1$"+5+ "s",df.format(totar[4][3] )) +"  "+ String.format("%1$"+5+ "s",df.format(totar[4][4]) )+"  "+String.format("%1$"+5+ "s",df.format(totar[4][5] ))+"  "+String.format("%1$"+5+ "s",df.format(totar[4][6] ))+"  "+String.format("%1$"+5+ "s",df.format(totar[4][7]) ) );






    }

    private void readNameFile(File file) {
        try {
            XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(file));
            sizeRange[0]= Float.valueOf(0);
            sizeName[6]= " UNUSED";
            for (Sheet sheet : wb ) {
                for (Row row : sheet) {
                    int rowId = row.getRowNum();
                    if(rowId != 0){

                        Cell point = row.getCell(0);
                        //threedims.setNames( point.getStringCellValue());
                        nameselement[rowId] = point.getStringCellValue();
                        Cell partition = row.getCell(1);
                        myarray[9][1][rowId] = (float) partition.getNumericCellValue();
                        Cell sizet = row.getCell(2);
                        if(rowId<=5){
                            size[rowId] = (float) sizet.getNumericCellValue()+ rowId;
                            sizeRange[rowId] = (float) sizet.getNumericCellValue();
                            sizeName[rowId] = String.valueOf(sizeRange[rowId-1] -sizeRange[rowId]);

                        }
                        Cell partMaxs = row.getCell(3);
                        if(rowId==1){
                            partMax =  (float) partMaxs.getNumericCellValue();
                        }






                    }
                }
            }
//            for (int i = 1; i < 6; i++) {
//                size[i] = 2+ i;
//            }
//            partMax = 10;
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private ArrayList<CCSEMStorageUnit>  readmagFile(File file, ArrayList<CCSEMStorageUnit> mag){
        try {
            XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(file));
            for (Sheet sheet : wb) {
                for (Row row : sheet) {
                    int rowId = row.getRowNum();
                    if (rowId != 0) {
                        storageUnit = new CCSEMStorageUnit();


                        Cell point = row.getCell(0);
                        storageUnit.setPoint((int) point.getNumericCellValue());
                        Cell ctype = row.getCell(1);
                        storageUnit.setCtype((int) ctype.getNumericCellValue());
                        Cell cts = row.getCell(2);
                        storageUnit.setCts((int) cts.getNumericCellValue());
                        Cell na = row.getCell(3);
                        storageUnit.setNA((float) na.getNumericCellValue());
                        Cell mg = row.getCell(4);

                        storageUnit.setMg((float) mg.getNumericCellValue());
                        Cell al = row.getCell(5);
                        storageUnit.setAl((float) al.getNumericCellValue());
                        Cell si = row.getCell(6);
                        storageUnit.setSi(Float.valueOf((float) si.getNumericCellValue()));
                        Cell p = row.getCell(7);
                        storageUnit.setP(Float.valueOf((float) p.getNumericCellValue()));
                        Cell s = row.getCell(8);
                        storageUnit.setS(Float.valueOf((float) s.getNumericCellValue()));
                        Cell cl = row.getCell(9);
                        storageUnit.setCl(Float.valueOf((float) cl.getNumericCellValue()));
                        Cell k = row.getCell(10);
                        storageUnit.setK(Float.valueOf((float) k.getNumericCellValue()));
                        Cell ca = row.getCell(11);
                        storageUnit.setCa(Float.valueOf((float) ca.getNumericCellValue()));
                        Cell fe = row.getCell(12);
                        storageUnit.setFe(Float.valueOf((float) fe.getNumericCellValue()));
                        Cell ba = row.getCell(13);
                        storageUnit.setBa(Float.valueOf((float) ba.getNumericCellValue()));
                        Cell ti = row.getCell(14);
                        storageUnit.setTi(Float.valueOf((float) ti.getNumericCellValue()));
                        Cell xcord = row.getCell(15);
                        storageUnit.setXcoord(Float.valueOf((float) xcord.getNumericCellValue()));
                        Cell ycord = row.getCell(16);
                        storageUnit.setYcoord(Float.valueOf((float) ycord.getNumericCellValue()));
                        Cell avgDia = row.getCell(17);
                        storageUnit.setAvgDiameter(Float.valueOf((float) avgDia.getNumericCellValue()));
                        Cell maxDiameter = row.getCell(18);
                        storageUnit.setMaxDiameter(Float.valueOf((float) maxDiameter.getNumericCellValue()));
                        Cell minDiameter = row.getCell(19);
                        storageUnit.setMinDiameter(Float.valueOf((float) minDiameter.getNumericCellValue()));
                        Cell area = row.getCell(20);
                        storageUnit.setArea(Float.valueOf((float) area.getNumericCellValue()));
                        Cell perim = row.getCell(21);
                        storageUnit.setPerim(Float.valueOf((float) perim.getNumericCellValue()));
                        Cell shape = row.getCell(22);
                        storageUnit.setShape(Float.valueOf((float) shape.getNumericCellValue()));

                        Cell frame = row.getCell(23);
                        storageUnit.setFrame((int) frame.getNumericCellValue());
                        Cell loclib = row.getCell(24);
                        storageUnit.setLocLib((int) loclib.getNumericCellValue());

                        mag.add(storageUnit);


                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mag;

    }
    private static void findSize(float diam, float area, float outVolume, int phasen, int locLib) {
        int category;
        if (diam < 2.2)
            category = 1;
        else if (diam < 4.6)
            category = 2;
        else if (diam < 10)
            category = 3;
        else if (diam < 22)
            category = 4;
        else if (diam < 46)
            category = 5;
        else
            category = 6;
        myarray[1][category][phasen] += outVolume;
        myarray[1][7][phasen] += outVolume;
        myarray[2][category][phasen] += 1;
        myarray[2][7][phasen] += 1;
        excludedCount[phasen]+= locLib;



    }

    /*
     * Material Fill
     *
     * Description:
     *
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 1994
     * Re-Modified by Binisha and copied in Java format
     * */
    private static void matFil( double coalArea, int cf) {
        float totalArea, totalDensity, sum;
        double weightCoalArea;

        for (int i = 1 ; i <= 33 ; ++i) {
            for (int j = 1; j <= 6; ++j) {
                if (j <= 3) {
                    myarray[7][j][i] = myarray[1][j][i] * cf;
                    System.out.println(myarray[7][j][i]+ " I am here");



                } else {
                    myarray[7][j][i] = myarray[1][j][i];
                    System.out.println(myarray[7][j][i]+ " I am here");




                }
                myarray[9][2][1] += myarray[7][j][i]; //totaling mineral areas
                myarray[9][3][1] += myarray[7][j][i] * myarray[9][1][i]; //area * density
                myarray[7][7][i] += myarray[7][j][i];



            }
        }
        weightCoalArea = coalArea - myarray[9][2][1];

        /*myarray(9, 2, 1) contails total corrected mineral area
         * myarray(9, 3, 1) contains total (area * density)*/
        //excluded: liberated
        totalArea = myarray[9][2][1];
        totalDensity= myarray[9][3][1];


        sum = (float) (totalDensity + ( weightCoalArea * 1.4));
        for (int j = 1; j <=33 ; j++) {
            for (int k = 1; k <=6 ; k++) {
                myarray[3][k][j] = ( myarray[7][k][j] /totalArea) * 100;
                myarray[4][k][j] = (float) ((( myarray[7][k][j] *  myarray[9][1][j])/totalDensity) * 100);
                myarray[5][k][j] = (float) (( myarray[7][k][j] /coalArea) * 100);
                myarray[6][k][j] = (( myarray[7][k][j] *  myarray[9][1][j])/ sum) * 100;



                if( myarray[7][7][j] != 0){
                    myarray[8][k][j] = myarray[7][k][j] / myarray[7][7][j] * 100;

                }
            }
        }

        for (int i = 1; i <= 33 ; i++) {
            for (int j = 3; j <=6 ; j++) {
                for (int k = 1; k <=6 ; k++) {
                    myarray[j][7][i] += myarray[j][k][i];
                }
            }
        }
        for (int i = 1; i <= 33 ; i++) {
            for (int j = 1; j <=6 ; j++) {
                myarray[8][7][i] += myarray[8][j][i];

            }
        }
        for (int i = 1; i <= 33 ; i++) {
            for (int j = 1; j <=7 ; j++) {
                for (int k = 1; k <=7 ; k++) {
                    totar[j][k] += myarray[j][k][i];
                }
            }
        }
        for (int i = 1; i <=7 ; i++) {
            totar[8][i] = totar[3][i];

        }



    }

}
