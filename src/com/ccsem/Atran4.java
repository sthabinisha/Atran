package com.ccsem;

import com.ccsem.common.Coalescence;

import java.io.File;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import static com.ccsem.App.getPhase;
import static com.ccsem.Main.fileIO;
import static com.ccsem.common.Data.sumFile;

/*
 * Atran4
 *
 * Description: Data manipulation of ash, similar to PARTCHAR
 *
 * * Author: Original by Tom Erickson, c version written by Shaker.
 * Modifications and testing done by Sean Allan 1994
 * Re-Modified by Binisha and copied in Java format
 * */
public class Atran4 {
//    public static Threedim threedim;
//    public static FileIO fileIO;
    public static int thresh, numFrame, low, chr, q, cf;
    public static float  partMax, epoxy, picArea, mag1, gr1;
    public static float[] size = new float[7];
    public static double field, coalArea;
    public static String[] infileName = {"LOCASH.xls", "LIBASH.xls"};
    public static String[] outFileName = {"LOCASHO.xls", "LIBASHO.xls"};
    public static String[] sumFileName = {"LOCASHS.PRN", "LIBASHS.PRN"};
    public static String[] sumPage = {"LOCASHSP.PRN", "LIBASHSP.PRN"};
    public static String[] sumFileNametemp = {"LOCASHSTEMP.PRN", "LIBASHSTEMP.PRN"};
    public static String[] sumPagetemp = {"LOCASHSPTEMP.PRN", "LIBASHSPTEMP.PRN"};

//    public static ArrayList<Threedim> mArray = new ArrayList<>();
    public static float[][][] myarray = new float[10][8][34];
    public static float[][] totar= new float[9][8];

    public  static String[] nameselement = new String[34];


//    public static ArrayList<Threedim.MArray> MArray = new ArrayList<>();

    private static DecimalFormat df = new DecimalFormat("0.00");

    public static ArrayList<Coalescence> locAshDataArrayList = new ArrayList<>();
    public static ArrayList<Coalescence> libAshDataArrayList = new ArrayList<>();
    public static final Float[][][] arrayThreedim = new Float[4][35][8];


//    public static LocAshData locAshData;
//    public static GetPhase getPhase;


    public static File inFile;
    public static File outFile;
//    public static String sumFile;
//    public static File sumPageFile;
    public static File sumFiletemp;
    public static File sumPageTempFile;

    public void ProcessAtran4(String[] nameselemen, float part, float[] size, float[][][] myarr, ArrayList<Coalescence> liberateData, ArrayList<Coalescence> coalesData, String user, String fundNo) {
        this.myarray = myarr;
        this.partMax = part;
        picArea = (float) ((Math.sqrt(115538.8) + 10.0) / 4.167);
        mag1 = 240;
        gr1 = 5;
        field = Math.pow((((picArea * 1000) / mag1) - (2 * gr1)), 2);
        System.out.println(field+ " field");
        epoxy = 50;
        chr = 1;
        thresh = 600;
        numFrame = 10;
        coalArea = ((((100 - epoxy) / 100) * field) * numFrame);
        System.out.println(coalArea+ "coalArea");
        System.out.println(myarray[9][1][3]+ " myarray first");
        libAshDataArrayList = readIN( liberateData, low, size);
        locAshDataArrayList = readIN( coalesData, low, size);

        nameselement = nameselemen;
        coalArea = ((((100 - epoxy) / 100) * field) * numFrame);
        System.out.println(coalArea+ "coalArea");

        cf = 1;

        matFil( coalArea, cf);

        // pumper(sumFile, )
        for (int i = 1; i <=2 ; i++) {
            pumper(low, numFrame, field, user, fundNo, epoxy, chr, nameselement);
        }




    }



    private ArrayList<Coalescence> readIN(ArrayList<Coalescence> locAshDataArrayList, int low, float[] size) {
        float avgDiameter, maxDiameter, minDiameter, inVolumer, outVolume, perim, shape, norm;
        double Na, Mg, Al, Si, P, S, Cl, K, CA, Fe, Ba, Ti;
        int frame, cts, type, point, phasen, counter, xc, yc;


        counter = 0;
        numFrame = -1;
        low = 0;
        //infile -> Lockasj.prn & Libash.prn

        //Converting equivalent % oxides to elemental analysis
        //some corrected 9/26/94 SEA
        for (int i = 0; i < locAshDataArrayList.size(); i++) {
            //  phasen = getPhase.getPhase(locAshDataArrayList.get(i).getAL(), locAshDataArrayList.get(i).getBA(), locAshDataArrayList.get(i).getCA(), locAshDataArrayList.get(i).getCL(), locAshDataArrayList.get(i).getFE(), locAshDataArrayList.get(i).getK(), locAshDataArrayList.get(i).getMG(), locAshDataArrayList.get(i).getNA(), locAshDataArrayList.get(i).getP(), locAshDataArrayList.get(i).getS(), locAshDataArrayList.get(i).getSI(), locAshDataArrayList.get(i).getTI());

            locAshDataArrayList.get(i).setNA((float) (locAshDataArrayList.get(i).getNA() * 0.7420)) ;
            locAshDataArrayList.get(i).setMg((float) (locAshDataArrayList.get(i).getMg() * 0.6032)) ;
            locAshDataArrayList.get(i).setAl((float) (locAshDataArrayList.get(i).getAl() *  0.5292)) ;
            locAshDataArrayList.get(i).setSi((float) (locAshDataArrayList.get(i).getSi() * 0.4675)) ;
            locAshDataArrayList.get(i).setP((float) (locAshDataArrayList.get(i).getP() *  0.4364)) ;
            locAshDataArrayList.get(i).setS((float) (locAshDataArrayList.get(i).getS() *  0.4005)) ;
            locAshDataArrayList.get(i).setCl((float) (locAshDataArrayList.get(i).getCl() *  0.6890)) ;
            locAshDataArrayList.get(i).setK((float) (locAshDataArrayList.get(i).getK() *  0.8300)) ;
            locAshDataArrayList.get(i).setCa((float) (locAshDataArrayList.get(i).getCa() * 0.7146)) ;
            locAshDataArrayList.get(i).setFe((float) (locAshDataArrayList.get(i).getFe() *  0.6994)) ;
            locAshDataArrayList.get(i).setTi((float) (locAshDataArrayList.get(i).getTi() *  0.5994));

            norm = locAshDataArrayList.get(i).getAl()+ locAshDataArrayList.get(i).getSi()+ locAshDataArrayList.get(i).getNA()+ locAshDataArrayList.get(i).getMg() +locAshDataArrayList.get(i).getP()+ locAshDataArrayList.get(i).getCl()+ locAshDataArrayList.get(i).getK() + locAshDataArrayList.get(i).getCa() + locAshDataArrayList.get(i).getFe()+ locAshDataArrayList.get(i).getBa() + locAshDataArrayList.get(i).getTi();

            if(norm <1) {
                norm = 1;
            }
            //float temp = 100/norm;
            locAshDataArrayList.get(i).setAl(locAshDataArrayList.get(i).getAl() *( 100/norm));
            locAshDataArrayList.get(i).setSi(locAshDataArrayList.get(i).getSi()* (100/norm));
            locAshDataArrayList.get(i).setNA(locAshDataArrayList.get(i).getNA()* (100/norm));
            locAshDataArrayList.get(i).setMg(locAshDataArrayList.get(i).getMg()* (100/norm));
            locAshDataArrayList.get(i).setP(locAshDataArrayList.get(i).getP() * (100/norm));
            locAshDataArrayList.get(i).setCl(locAshDataArrayList.get(i).getCl() *( 100/norm));
            locAshDataArrayList.get(i).setK(locAshDataArrayList.get(i).getK() * (100/norm));
            locAshDataArrayList.get(i).setCa(locAshDataArrayList.get(i).getCa() * (100/norm));
            locAshDataArrayList.get(i).setFe(locAshDataArrayList.get(i).getFe() * (100/norm));
            locAshDataArrayList.get(i).setTi(locAshDataArrayList.get(i).getTi() * (100/norm));
            locAshDataArrayList.get(i).setS(locAshDataArrayList.get(i).getS() * (100/norm));
            ++counter;
            if(locAshDataArrayList.get(i).getAvgdiameter()<= partMax){
                if(locAshDataArrayList.get(i).getFrame()> numFrame){
                    numFrame = locAshDataArrayList.get(i).getFrame();

                }
                if(locAshDataArrayList.get(i).getCts()<thresh)
                    ++low;
                else{
                    if(locAshDataArrayList.get(i).getAl() == 0)
                        locAshDataArrayList.get(i).setAl((float) 0.0000001);
                    if(locAshDataArrayList.get(i).getS() ==0)
                        locAshDataArrayList.get(i).setS((float) 0.0000001);

                    phasen = getPhase.getPhase(locAshDataArrayList.get(i).getAl(), locAshDataArrayList.get(i).getBa(), locAshDataArrayList.get(i).getCa(), locAshDataArrayList.get(i).getCl(), locAshDataArrayList.get(i).getFe(), locAshDataArrayList.get(i).getK(), locAshDataArrayList.get(i).getMg(), locAshDataArrayList.get(i).getNA(), locAshDataArrayList.get(i).getP(), locAshDataArrayList.get(i).getS(), locAshDataArrayList.get(i).getSi(), locAshDataArrayList.get(i).getTi());
                    locAshDataArrayList.get(i).setAvgdiameter((float) (locAshDataArrayList.get(i).getAvgdiameter() * Math.pow(myarray[9][1][phasen], (1/2))));
                    outVolume = locAshDataArrayList.get(i).getPv();
                    locAshDataArrayList.get(i).setPhase(phasen);
                    //outFile -> Locasho.prn & lbasho.prn
                   // writeAshFile(outFile,locAshDataArrayList.get(i).getPoints(), locAshDataArrayList.get(i).getCounts(), locAshDataArrayList.get(i).getNA(), locAshDataArrayList.get(i).getMG(), locAshDataArrayList.get(i).getAL(), locAshDataArrayList.get(i).getSI(), locAshDataArrayList.get(i).getP(), locAshDataArrayList.get(i).getS(), locAshDataArrayList.get(i).getCL(), locAshDataArrayList.get(i).getK(), locAshDataArrayList.get(i).getCA(), locAshDataArrayList.get(i).getFE(), locAshDataArrayList.get(i).getBA(), locAshDataArrayList.get(i).getTI(), locAshDataArrayList.get(i).getXcoord(), locAshDataArrayList.get(i).getYcoord(), locAshDataArrayList.get(i).getDiam(), outVolume, locAshDataArrayList.get(i).getShape(), locAshDataArrayList.get(i).getFrame(), phasen );
                    findSize(locAshDataArrayList.get(i).getAvgdiameter(), outVolume, phasen, size);
                }
            }



        }
        return locAshDataArrayList;
    }
    /*
     * Find Size
     *
     * Description: Classifies the average diameter into six intervals 1-2.2, 2.2-4.6, 4.6-10, 10-22, 22-46, 46-100um
     *
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 1994
     * Re-Modified by Binisha and copied in Java format
     * */
    private static void findSize(float diam, float outVolume,  int phasen, float[] size) {
        int category;
        if(diam <size[1])
            category = 1;
        else if(diam <size[2])
            category = 2;
        else if(diam <size[3])
            category = 3;
        else if(diam <size[4])
            category = 4;
        else if(diam <size[5])
            category = 5;
        else
            category = 6;
        System.out.println(category+ " p "+ phasen);
        myarray[1][category][phasen] += outVolume;
        myarray[1][7][phasen] += outVolume;
        myarray[2][category][phasen] += 1;
        myarray[2][7][phasen] += 1;



    }
    /*
     * Material Fill
     *
     * Description:
     *
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 1994
     * Re-Modified by Binisha and copied in Java format
     * */
    private static void matFil( double coalArea, int cf) {
        float totalArea, totalDensity, sum;
        double weightCoalArea;
        for (int i = 1 ; i <= 33 ; ++i) {
            for (int j = 1; j <= 6; ++j) {
                if (j <= 3) {
                    myarray[7][j][i] = myarray[1][j][i] * cf;
                    System.out.println(myarray[7][j][i]+ " yeta");

                } else {
                    myarray[7][j][i] = myarray[1][j][i];
                    System.out.println(myarray[7][j][i]+ " yeta ki");



                }
                myarray[9][2][1] += myarray[7][j][i]; //totaling mineral areas
                myarray[9][3][1] += myarray[7][j][i] * myarray[9][1][i]; //area * density
                myarray[7][7][i] += myarray[7][j][i];

            }
        }
        weightCoalArea = coalArea - myarray[9][2][1];
        System.out.println(weightCoalArea+ " weightCoalArea");

        /*myarray(9, 2, 1) contails total corrected mineral area
         * myarray(9, 3, 1) contains total (area * density)*/
        totalArea = myarray[9][2][1];
        System.out.println(totalArea+ " totalArea");

        totalDensity= myarray[9][3][1];
        System.out.println(totalDensity+ " totalDensity");


        sum = (float) (totalDensity + ( weightCoalArea * 1.4));
        for (int j = 1; j <=33 ; j++) {
            for (int k = 1; k <=6 ; k++) {
                myarray[3][k][j] = ( myarray[7][k][j] /totalArea) * 100;
                myarray[4][k][j] = (( myarray[7][k][j] *  myarray[9][1][j])/ totalDensity) * 100;
                myarray[5][k][j] = (float) (( myarray[7][k][j] /coalArea) * 100);
                myarray[6][k][j] = (( myarray[7][k][j] *  myarray[9][1][j])/ sum) * 100;
                if( myarray[7][7][j] != 0){
                    myarray[8][k][j] = myarray[7][k][j] / myarray[7][7][j] * 100;
                }
            }
        }

        for (int i = 1; i <= 33 ; i++) {
            for (int j = 3; j <=6 ; j++) {
                for (int k = 1; k <=6 ; k++) {
                    myarray[j][7][i] += myarray[j][k][i];
                }
            }
        }
        for (int i = 1; i <= 33 ; i++) {
            for (int j = 1; j <=6 ; j++) {
                myarray[8][7][i] += myarray[8][j][i];

            }
        }
        for (int i = 1; i <= 33 ; i++) {
            for (int j = 1; j <=7 ; j++) {
                for (int k = 1; k <=7 ; k++) {
                    totar[j][k] += myarray[j][k][i];
                }
            }
        }
        for (int i = 1; i <=7 ; i++) {
            totar[8][i] = totar[3][i];

        }



    }
    /*
     * Pumper
     *
     * Description: Puts header on the summary files
     *
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 1994
     * Re-Modified by Binisha and copied in Java format
     * */
    private static void pumper(int low, int numFrame, double field, String user, String fundNo, float epoxy, int chr, String[] nameselement) {

        int l, n, i, sp1, sp2;
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        //l is used to space the proper number of lines
        i=1;
        sp1 = 8;
        sp2 = 10;

        do{
//            if(i ==2){
//                sumFile = sumPage; // pointless
//                sumTempFile = sumPagetemp;
//            }
            l = 1;
//            fileIO.tprintf(sumFile, "Summary of ASH PREDICTION results: PROG VERSION 2BF " + dtf.format(now));
//            fileIO.tprintf(sumFile, "SAMPLE DESCRIPTION ----> " + user);
//            fileIO.tprintf(sumFile, "SUBMITTER          ----> " + user);
//            fileIO.tprintf(sumFile, "ICC # AND FUND #   ----> " + fundNo);
//            fileIO.tprintf(sumFile, "RUN DATE AND TIME  ----> " + dtf.format(now));
//            fileIO.tprintf(sumFile, "SUMMARY OF PARAMETERS " );


            if(chr != 1) {
//                fileIO.tprintf(sumFile, "PERCENT EPOXY USED      " + epoxy);
            }
//            fileIO.tprintf(sumFile, "TOTAL MINERAL AREA ANALYZED    ----> " + totar[1][7]);
//            fileIO.tprintf(sumFile, "FIELD SIZE USED                 ----> " + field);
//            fileIO.tprintf(sumFile, "NUMBER OF FRAMES                ----> " + numFrame);
            l = 5;


            if(chr == 1) {

//                fileIO.tprintf(sumFile, "TOTAL NUMBER OF POINTS ANALYSED       ----> " + (int) totar[2][7]);
//                fileIO.tprintf(sumFile, "NUMBER OF POINTS UNDER THRESHOLD      ----> " + low);

                ++l;

            }else{
//                fileIO.tprintf(sumFile, "TOTAL MINERAL AREA ON A COAL BASIS     ----> " + totar[5][7]);
//                fileIO.tprintf(sumFile, "TOTAL MINERAL WEIGHT % ON A COAL BASIS ----> " + totar[6][7]);
//
//                fileIO.tprintf(sumFile, "TOTAL NUMBER OF POINTS ANALYZED        ----> " + totar[2][7]);
//
//                fileIO.tprintf(sumFile, "NUMBER OF POINTS UNDER THRESHOLD       ----> " + low);
            }
//            fileIO.tprintf(sumFile, "\n \n WEIGHT PERCENT ON A MINERAL BASIS " );
            writeSumFile(sumFile, 4, myarray, i);

            if(i==1){
                n= 1;
//                fileIO.tprintf(sumFile, "\n \n PAGE " + n + " " + nameselement[0] );
//                fileIO.tprintf(sumFile, "\n \n area in each size range\n " );
                //writeSumFile(sumFile,  1, myarray, i);
                ++n;



            }
            if(i==1)
                i= 2;
            else
                i = 0;// just something to get use out of here SA



        }while(i ==2);
    }

    /*
     * Write sumFile
     *
     * Description: Outputs the summary files Locashs.prn, Libash.prn
     * Locashsp, Libashsp.prn
     *
     * * Author: Original by Tom Erickson, c version written by Shaker.
     * Modifications and testing done by Sean Allan 1994
     * Re-Modified by Binisha and copied in Java format
     * */
    private static void writeSumFile(String s, int tablen, float[][][] mArray, int m) {
//        fileIO.tprintf(s,  String.format("%1$"+27+ "s", "1.0")+ "    2.2     4.6    10    22      46  " );
//        fileIO.tprintf(s, String.format("%1$"+27+ "s", "TO")+ "      TO     TO     TO    TO     TO    TOTALS" );
//        fileIO.tprintf(s, String.format("%1$"+27+ "s", "2.2")+ "     4.6     10     22    46     100" );
//        fileIO.tprintf(s, "------------------------------------------\n" );

        if((tablen ==5)||tablen ==6){
            //it can never happen
        }else {
            for (int i = 1; i <=33; i++) {
                //fileIO.easierToreadforatran7(stemp, /*nameselement[i].replaceAll("\\s+", "_")+ " "+*/  myarray[tablen][1][i] + " "+  myarray[tablen][2][i] + " "+  myarray[tablen][3][i] + " "+  myarray[tablen][4][i] + " "+  myarray[tablen][5][i] + " "+  myarray[tablen][6][i] + " "+  myarray[tablen][7][i] + " ");
               // fileIO.tprintfn(s, String.format("%1$"+20+ "s", nameselement[i]+ " yesma"));

              //  fileIO.tprintf(s, "  " + String.format("%1$"+5+ "s", df.format(myarray[tablen][1][i]))+ "  " +String.format("%1$"+5+ "s", df.format(myarray[tablen][2][i]))+ "  " + String.format("%1$"+5+ "s", df.format(myarray[tablen][3][i]))+ "  " + String.format("%1$"+5+ "s", df.format(myarray[tablen][4][i]))+ "  " + String.format("%1$"+5+ "s", df.format(myarray[tablen][5][i]))+ "  " + String.format("%1$"+5+ "s", df.format(myarray[tablen][6][i]))+ "  " + String.format("%1$"+5+ "s", df.format(myarray[tablen][7][i])));
                for (int j = 1; j <= 7; j++) {
                    arrayThreedim[m][i][j] = myarray[tablen][j][i];


                }
            }
        }
       // fileIO.tprintf(s, "------------------------------------------\n" );
        for (int j = 1; j <= 7; j++) {
            arrayThreedim[m][34][j] = totar[tablen][j];


        }

        //  fileIO.easierToreadforatran7(stemp,df.format(totar[tablen][1]) +"  "+df.format(totar[tablen][2] ) +"  "+df.format(totar[tablen][3] ) +"  "+ df.format(totar[tablen][4]) +"  "+df.format(totar[tablen][5] )+"  "+df.format(totar[tablen][6] )+"  "+df.format(totar[tablen][7]) );
        //fileIO.tprintf(s, String.format("%1$"+20+ "s", "TOTALS")+ "   "  + String.format("%1$"+5+ "s",df.format(totar[tablen][1] ))+"  "+String.format("%1$"+5+ "s",df.format(totar[tablen][2] )) +"  "+String.format("%1$"+5+ "s",df.format(totar[tablen][3] )) +"  "+ String.format("%1$"+5+ "s",df.format(totar[tablen][4]) )+"  "+String.format("%1$"+5+ "s",df.format(totar[tablen][5] ))+"  "+String.format("%1$"+5+ "s",df.format(totar[tablen][6] ))+"  "+String.format("%1$"+5+ "s",df.format(totar[tablen][7]) ) );







    }

}
